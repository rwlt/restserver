#ifndef SRC_APP_CPPUTEST_UPLOADTEST_H_
#define SRC_APP_CPPUTEST_UPLOADTEST_H_


#include <microhttpd.h>
#include "RestServer.h"
#include "PersonalModel.h"
#include "nholmann/json.hpp"
#include "restclient-cpp/restclient.h"
#include "restclient-cpp/connection.h"
#include "hashalgs/HMACGenerator.h"
#include "hashalgs/NonceGenerator.h"
#include "hashalgs/DateTimeStamp.h"
#include "hashalgs/Base64.h"

#include <random>
#include <thread>
#define PORT 8888
constexpr const char *SERVER_URL = "http://localhost:8888";


using json = nlohmann::json;
using Module::PA;
using Module::PersonalModel;

auto names = std::vector<std::string>{"Gary", "Barry", "Carl", "John", "Brian", "David", "Boll", "Carry", "Sue", "Delia"};
std::default_random_engine generator;
std::uniform_int_distribution<int> distribution(0, 9);

json peopleCreater(int startID, int amt)
{
  json peopleL1;
  peopleL1[PA.T.People] = json::array({});

  // Fake names
  auto itemran = std::bind(distribution, generator);

  for (int i = startID; i < (startID + amt); i++)
  {
    auto r1 = itemran();
    auto r2 = itemran();
    auto name = names[r1] + " " + names[r2];
    json person;
    person[PA.F.ID] = i;
    person[PA.F.Name] = name;
    peopleL1[PA.T.People].push_back(person);
  }
  return peopleL1;
};

#endif /* SRC_APP_CPPUTEST_UPLOADTEST_H_ */
