/*
 * UploadTest.cpp
 *
 *  Created on: 5/06/2017
 *      Author: rodney
 */
#include "UploadTest.h"
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// UploadRestAccessor test group
//-------------------------------------------------------
//
TEST_GROUP(UploadRestAccessor)
{
  PersonalModel pb{true};
  struct MHD_Daemon *daemon;
  RestServer restServer;

  void setup()
  {
    // Fake names
    auto itemran = std::bind(distribution, generator);
    pb.beginTransaction();
    for (int i = 0; i < 100; i++)
    {
      auto r1 = itemran();
      auto r2 = itemran();
      auto name = names[r1] + " " + names[r2];
      pb.addNewPerson(i, name);
    }
    pb.endTransaction();

    restServer.get("/persons", [this](const HttpRequest &)
                   {
                     std::unique_ptr<HttpResponse> res = std::make_unique<HttpResponse>();
                     std::cerr << "GET PERSONS METHOD\n";
                     json root;
                     auto list = pb.doRead();
                     std::cerr << "Size: " << list.size() << "\n";
                     root[PA.T.People] = json::array({});
                     for (auto &p : list)
                     {
                       json person;
                       auto pID = std::get<0>(p->get(PA.F.ID));
                       auto pName = std::get<0>(p->get(PA.F.Name));
                       person[PA.F.ID] = pID;
                       person[PA.F.Name] = pName;
                       root[PA.T.People].push_back(person);
                     }
                     std::string document = root.dump();
                     res->set_status(Rest::ResponseIdentity::OK);
                     res->add_header(std::string(Rest::CONTENT_TYPE), std::string(Rest::APPLICATION_JSON));
                     res->set_body(document);
                     return res;
                   });

    restServer.put("/loadpeople", [this](const HttpRequest &req)
                   {
                     std::unique_ptr<HttpResponse> res = std::make_unique<HttpResponse>();

                     std::cerr << "PUT LOAD PEOPLE METHOD\n";
                     json root;
                     json errs;
                     errs["errors"] = json::array({}); // accumalate errs and set Request object then

                     std::cerr << "HEADERS:\n";
                     for (auto h : req.headers())
                     {
                       std::cerr << h.first << " " << h.second << "\n";
                     }
                     std::vector<std::string> errList;
                     std::cerr << "BODY:" << req.body();
                     try
                     {
                       root = json::parse(req.body());
                     }
                     catch (json::exception &e)
                     {
                       errList.push_back(std::to_string(e.id) + " " + e.what());
                     }
                     if (errList.size() > 0)
                     {
                       res->set_status(Rest::ResponseIdentity::NotAccepted);
                       for (auto err : errList)
                       {
                         json msg;
                         msg["message"] = err;
                         errs["errors"].push_back(msg);
                       }
                       res->add_header(std::string(Rest::CONTENT_TYPE), std::string(Rest::APPLICATION_JSON));
                       res->set_body(errs.dump());
                     }
                     else
                     {
                       //  Just sending back what was sent to work
                       std::string document = root.dump();
                       res->set_status(Rest::ResponseIdentity::OK);
                       res->add_header(std::string(Rest::CONTENT_TYPE), std::string(Rest::APPLICATION_JSON));
                       res->set_body(document);
                     }
                     return res;
                   });

    daemon = MHD_start_daemon(/*MHD_USE_THREAD_PER_CONNECTION |*/
                              MHD_USE_INTERNAL_POLLING_THREAD,
                              /* MHD_USE_ERROR_LOG| MHD_USE_SSL*/
                              PORT,
                              NULL, NULL,
                              rest_connection, &restServer,
                              //MHD_OPTION_NOTIFY_COMPLETED, &RestServer::request_completed, NULL,
                              MHD_OPTION_END);

    if (NULL == daemon)
    {
      LONGS_EQUAL(0, 1);
    }
  }

  void teardown()
  {
    MHD_stop_daemon(daemon);
  }
};

TEST(UploadRestAccessor, RestGetWithHMacOnHttps)
{

  // initialize RestClient
  RestClient::init();
  RestClient::Connection conn(SERVER_URL);
  //conn.SetCAInfoFilePath("certfile");

  std::string user = "rod";
  std::string password = "pa$$w0rd";
  std::string apikey = "213019FF46E0577C6BECEC9966F0426C";

  NonceGenerator ng;
  HMACGenerator hmacGen;

  std::string sharedsecret = ng.secretKeyGenerate(password, apikey);
  std::string nonce = ng.generate();

  std::cout << "user rod secret: " << sharedsecret << "\n";
  std::string method = "GET";
  std::string resurl = "/persons";
  std::string currenttime = DateTimeStamp::currentDateTime();
  std::string tohash{user + "+" + method + "+" + resurl + "+" + currenttime + "+" + nonce};
  std::cout << "HMAC Hash: " << tohash << "\n";

  hmacGen.generate(sharedsecret, tohash);
  std::string genB64 = Base64::base64_encode(hmacGen.getSignatureBytes(), hmacGen.getSignatureSize());
  std::string authHead{user + ":" + nonce + ":" + genB64};
  std::cout << "Authorize " << authHead << "\n";
  conn.AppendHeader("Authorization", "hmac " + authHead);
  conn.AppendHeader("Date", currenttime);
  conn.AppendHeader("Accept", "application/json");
  conn.AppendHeader("Content-Type", "application/json");

  RestClient::Response rx = conn.get("/persons");
  std::stringstream heads;
  for (auto h : rx.headers)
  {
    heads << " [" << h.first << "] " << h.second;
  }
  std::cerr << heads.str() << "\n[" << rx.body << "]\n";

  CHECK(!rx.headers["HTTP/1.1 200 OK"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 200 OK"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());
  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
    std::cerr << errs << "\n";
  }
  CHECK(errs.empty());

  if (root.contains(PA.T.People) && root[PA.T.People].is_array())
  {
    LONGS_EQUAL(100, root[PA.T.People].size());
    // auto first = root[PA.T.People].at(0);
    // if (first.contains(PA.F.Name) && first[PA.F.Name].is_string())
    // {
    //   STRCMP_EQUAL("Brian Bary", first[PA.F.Name].get<std::string>().c_str());
    // }
  }
  else
  {
    CHECK(false);
  }
};

TEST(UploadRestAccessor, AnswerConnectionPutJsonAuthProcessAccessor)
{
  std::this_thread::sleep_for(std::chrono::milliseconds(200));

  json peopleL4 = peopleCreater(4000, 50);

  RestClient::init();
  RestClient::Connection conn(SERVER_URL);
  RestClient::Response rx = conn.put("/loadpeople", peopleL4.dump());
  // Check the one connection
  std::stringstream heads;
  for (auto h : rx.headers)
  {
    heads << " [" << h.first << "] " << h.second;
  }
  std::cerr << heads.str() << "\n"; //[" << rx.body << "]\n";
  CHECK(!rx.headers["HTTP/1.1 200 OK"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 200 OK"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());
  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
    std::cerr << errs << "\n";
  }
  CHECK(errs.empty());
  if (root.contains(PA.T.People) && root[PA.T.People].is_array())
  {
    LONGS_EQUAL(50, root[PA.T.People].size());
  }
  else
  {
    CHECK(false);
  }
};
