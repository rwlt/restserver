/*
 * RestClientTest.cpp
 *
 *  Created on: 5/06/2017
 *      Author: rodney
 */

#include <microhttpd.h>
#include "restclient-cpp/restclient.h"
#include "restclient-cpp/connection.h"
#include "RestServer.h"
#include "PersonalModel.h"
#include "RestRequest.h"
#include "hashalgs/HMACGenerator.h"
#include "hashalgs/NonceGenerator.h"
#include "hashalgs/DateTimeStamp.h"
#include "hashalgs/Base64.h"
#include "nholmann/json.hpp"
#include <vector>
#include <string>
#include <iostream>
#include <thread>
#include <cstring>

using json = nlohmann::json;
using Module::PA;
using Module::PersonalModel;

#define PORT 8888
//#define MAXNAMESIZE 20
//#define MAXANSWERSIZE 512
constexpr const char *SERVER_URL = "http://localhost:8888";
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// RestClientSecureAccessor test group
//-------------------------------------------------------
//
TEST_GROUP(RestClientSecureAccessor)
{
  struct MHD_Daemon *daemon;
  char *key_pem;
  char *cert_pem;
  RestServer restServer;
  PersonalModel pb{true};

  void setup()
  {
    key_pem = load_file("server.key");
    cert_pem = load_file("server.pem");

    if ((key_pem == NULL) || (cert_pem == NULL))
    {
      printf("The key/certificate files could not be read.\n");
      LONGS_EQUAL(0, 1);
    }

    auto p1 = pb.addNewPerson(100, "Brian Bary");
    auto p2 = pb.addNewPerson(200, "Jack Brian");
    auto p3 = pb.addNewPerson(300, "Carl Boll");
    auto p4 = pb.addNewPerson(400, "David Smith");
    ResourceCall pcall = [](PersonalModel *db, std::string /*pass*/, std::string /*json*/) -> ResourceCallReturn {
      json root;
      auto list = db->doRead();
      root[PA.T.People] = json::array({});
      for (auto &p : list)
      {
        json person;
        auto pID = std::get<0>(p->get(PA.F.ID));
        auto pName = std::get<0>(p->get(PA.F.Name));
        person[PA.F.ID] = pID;
        person[PA.F.Name] = pName;
        root[PA.T.People].push_back(person);
      }
      std::string document = root.dump();
      ResourceCallReturn result(true, document);
      return result;
    };

    restServer.addResource("/persons", pcall);

    daemon = MHD_start_daemon(MHD_USE_INTERNAL_POLLING_THREAD,
                              PORT,
                              NULL, NULL,
                              &RestServer::connection, &restServer,
                              //MHD_OPTION_NOTIFY_COMPLETED, &RestServer::request_completed, NULL,
                              MHD_OPTION_END);

    if (NULL == daemon)
    {
      printf("%s\n", cert_pem);

      free(key_pem);
      free(cert_pem);
      LONGS_EQUAL(0, 1);
    }
  }

  void teardown()
  {
    MHD_stop_daemon(daemon);
    free(key_pem);
    free(cert_pem);
  }
};

TEST(RestClientSecureAccessor, RestGetWithHMacOnHttps)
{

  // initialize RestClient
  RestClient::init();
  RestClient::Connection conn(SERVER_URL);
  //conn.SetCAInfoFilePath("certfile");

  std::string user = "rod";
  std::string password = "pa$$w0rd";
  std::string apikey = "213019FF46E0577C6BECEC9966F0426C";

  NonceGenerator ng;
  HMACGenerator hmacGen;

  std::string sharedsecret = ng.secretKeyGenerate(password, apikey);
  std::string nonce = ng.generate();

  std::cout << "user rod secret: " << sharedsecret << "\n";
  std::string method = "GET";
  std::string resurl = "/persons";
  std::string currenttime = DateTimeStamp::currentDateTime();
  std::string tohash{user + "+" + method + "+" + resurl + "+" + currenttime + "+" + nonce};
  std::cout << "HMAC Hash: " << tohash << "\n";

  hmacGen.generate(sharedsecret, tohash);
  std::string genB64 = Base64::base64_encode(hmacGen.getSignatureBytes(), hmacGen.getSignatureSize());
  std::string authHead{user + ":" + nonce + ":" + genB64};
  std::cout << "Authorize " << authHead << "\n";
  conn.AppendHeader("Authorization", "hmac " + authHead);
  conn.AppendHeader("Date", currenttime);
  conn.AppendHeader("Accept", "application/json");
  conn.AppendHeader("Content-Type", "application/json");

  RestClient::Response rx = conn.get("/persons");

  CHECK(!rx.headers["HTTP/1.1 200 OK"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 200 OK"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());
  //std::cerr << "body " << rx.body << "\n";
  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
    std::cerr << errs << "\n";
  }
  CHECK(errs.empty());

  if (root.contains(PA.T.People) && root[PA.T.People].is_array())
  {
    LONGS_EQUAL(4, root[PA.T.People].size());
    auto first = root[PA.T.People].at(0);
    if (first.contains(PA.F.Name) && first[PA.F.Name].is_string())
    {
      STRCMP_EQUAL("Brian Bary", first[PA.F.Name].get<std::string>().c_str());
    }
  }
  else
  {
    CHECK(false);
  }

  //std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

TEST(RestClientSecureAccessor, AnswerConnectionPostJsonProcessAccessor)
{
  RestClient::init();
  RestClient::Connection conn(SERVER_URL);
  //conn.SetCAInfoFilePath("certfile");

  std::string user = "rod";
  std::string password = "pa$$w0rd";
  std::string apikey = "213019FF46E0577C6BECEC9966F0426C";

  NonceGenerator ng;
  std::string sharedsecret = ng.secretKeyGenerate(password, apikey);
  std::string nonce = ng.generate();
  std::string method = "POST";
  std::string resurl = "/persons";
  std::string currenttime = DateTimeStamp::currentDateTime();
  std::string tohash;
  tohash = user + "+" + method + "+" + resurl + "+" + currenttime + "+" + nonce;
  std::cout << "PostJson To Hash: " << tohash << "\n";
  HMACGenerator hmacGen;
  hmacGen.generate(sharedsecret, tohash);
  std::string genB64 = Base64::base64_encode(hmacGen.getSignatureBytes(), hmacGen.getSignatureSize());
  std::string authHead;
  authHead = user + ":" + nonce + ":" + genB64;
  conn.AppendHeader("Authorization", "hmac " + authHead);
  conn.AppendHeader("Date", currenttime);
  conn.AppendHeader("Accept", "application/json");
  conn.AppendHeader("Content-Type", "application/json");

  json person;
  person[PA.F.ID] = "300";
  person[PA.F.Name] = "Sam Cash";

  RestClient::Response rx = conn.post("/persons", person.dump());
  std::stringstream heads;
  for (auto h : rx.headers)
  {
    heads << " [" << h.first << "] " << h.second;
  }
  std::cerr << "Headers " << heads.str() << "\n";

  CHECK(!rx.headers["HTTP/1.1 200 OK"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 200 OK"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());
  //std::cerr << "body " << rx.body << "\n";
  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
    std::cerr << errs << "\n";
  }
  CHECK(errs.empty());

  if (root.contains(PA.F.Name) && root[PA.F.Name].is_string())
  {
    STRCMP_EQUAL("Sam Cash", root[PA.F.Name].get<std::string>().c_str());
  }
  else
  {
    CHECK(false);
  }

  //std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

TEST(RestClientSecureAccessor, AnswerConnectionPostJsonProcessAccessorEmptyPost)
{
  RestClient::init();
  RestClient::Connection conn(SERVER_URL);
  //conn.SetCAInfoFilePath("certfile");

  std::string user = "rod";
  std::string password = "pa$$w0rd";
  std::string apikey = "213019FF46E0577C6BECEC9966F0426C";

  NonceGenerator ng;
  std::string sharedsecret = ng.secretKeyGenerate(password, apikey);
  std::string nonce = ng.generate();
  std::string method = "POST";
  std::string resurl = "/persons";
  std::string currenttime = DateTimeStamp::currentDateTime();
  std::string tohash;
  tohash = user + "+" + method + "+" + resurl + "+" + currenttime + "+" + nonce;
  std::cout << "PostJson To Hash: " << tohash << "\n";
  HMACGenerator hmacGen;
  hmacGen.generate(sharedsecret, tohash);
  std::string genB64 = Base64::base64_encode(hmacGen.getSignatureBytes(), hmacGen.getSignatureSize());
  std::string authHead;
  authHead = user + ":" + nonce + ":" + genB64;
  conn.AppendHeader("Authorization", "hmac " + authHead);
  conn.AppendHeader("Date", currenttime);
  conn.AppendHeader("Accept", "application/json");
  conn.AppendHeader("Content-Type", "application/json");

  //std::cerr << "&&&&&&&&&&&&&&&&&&&&&&&&&&&& Valid user empty post\n";
  json person;
  person[PA.F.ID] = "300";
  person[PA.F.Name] = "Sam Cash";

  RestClient::Response rx = conn.post("/persons", "");
  std::stringstream heads;
  for (auto h : rx.headers)
  {
    heads << " [" << h.first << "] " << h.second;
  }
  std::cerr << "Headers " << heads.str() << "\n[" << rx.body << "] \n";

  CHECK(!rx.headers["HTTP/1.1 406 Not Acceptable"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 406 Not Acceptable"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());
  //std::cerr << "body " << rx.body << "\n";
  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
    std::cerr << errs << "\n";
  }
  CHECK(errs.empty());

  if (root.contains("errors") && root["errors"].is_array())
  {
    STRCMP_EQUAL("Empty post not allowed: POST", root["errors"][0]["message"].get<std::string>().c_str());
  }
  else
  {
    CHECK(false);
  }

  //std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

TEST(RestClientSecureAccessor, AnswerConnectionPostBadJsonProcessAccessor)
{
  RestClient::init();
  RestClient::Connection conn(SERVER_URL);
  //conn.SetCAInfoFilePath("certfile");

  std::string user = "rod";
  std::string password = "pa$$w0rd";
  std::string apikey = "213019FF46E0577C6BECEC9966F0426C";

  NonceGenerator ng;
  std::string sharedsecret = ng.secretKeyGenerate(password, apikey);
  std::string nonce = ng.generate();
  std::string method = "POST";
  std::string resurl = "/persons";
  std::string currenttime = DateTimeStamp::currentDateTime();
  std::string tohash;
  tohash = user + "+" + method + "+" + resurl + "+" + currenttime + "+" + nonce;
  std::cout << "PostJson To Hash: " << tohash << "\n";
  HMACGenerator hmacGen;
  hmacGen.generate(sharedsecret, tohash);
  std::string genB64 = Base64::base64_encode(hmacGen.getSignatureBytes(), hmacGen.getSignatureSize());
  std::string authHead;
  authHead = user + ":" + nonce + ":" + genB64;
  conn.AppendHeader("Authorization", "hmac " + authHead);
  conn.AppendHeader("Date", currenttime);
  conn.AppendHeader("Accept", "application/json");
  conn.AppendHeader("Content-Type", "application/json");

  RestClient::Response rx = conn.post("/persons", "{oo\":\"bla\"}");
  CHECK(!rx.headers["HTTP/1.1 406 Not Acceptable"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 406 Not Acceptable"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());
  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
  }
  CHECK(errs.empty());

  if (root.contains("errors") && root["errors"].is_array())
  {
    STRCMP_EQUAL("101 [json.exception.parse_error.101] parse error at line 1, column 2: syntax error while parsing object key - invalid literal; last read: '{o'; expected string literal", root["errors"][0]["message"].get<std::string>().c_str());
  }
  else
  {
    CHECK(false);
  }
  //std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

TEST(RestClientSecureAccessor, AnswerConnectionPostJsonUnauthProcessAccessor)
{
  RestClient::init();
  RestClient::Connection conn(SERVER_URL);
  std::string method = "POST";
  std::string resurl = "/persons";
  std::string currenttime = DateTimeStamp::currentDateTime();
  conn.AppendHeader("Date", currenttime);
  conn.AppendHeader("Accept", "application/json");
  conn.AppendHeader("Content-Type", "application/json");

  RestClient::Response rx = conn.post("/persons", "{\"foo\":\"bla\"}");
  std::stringstream heads;
  for (auto h : rx.headers)
  {
    heads << " [" << h.first << "] " << h.second;
  }
  std::cerr << heads.str() << "\n";
  CHECK(!rx.headers["HTTP/1.1 401 Unauthorized"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 401 Unauthorized"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());

  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
  }
  CHECK(errs.empty());

  if (root.contains("errors") && root["errors"].is_array())
  {
    STRCMP_EQUAL("Unauthorised access", root["errors"][0]["message"].get<std::string>().c_str());
  }
  else
  {
    CHECK(false);
  }

  //std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

TEST(RestClientSecureAccessor, AnswerConnectionPostJsonUnauthProcessAccessorEmptyBody)
{
  RestClient::init();
  RestClient::Connection conn(SERVER_URL);
  std::string method = "POST";
  std::string resurl = "/persons";
  std::string currenttime = DateTimeStamp::currentDateTime();
  conn.AppendHeader("Date", currenttime);
  conn.AppendHeader("Accept", "application/json");
  conn.AppendHeader("Content-Type", "application/json");
  //std::cerr << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
  RestClient::Response rx = conn.post("/persons", "");
  std::stringstream heads;
  for (auto h : rx.headers)
  {
    heads << " [" << h.first << "] " << h.second;
  }
  std::cerr << "ProcessAccessorEmptyBody " << heads.str() << "\n[" << rx.body << "]\n";

  CHECK(!rx.headers["HTTP/1.1 401 Unauthorized"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 401 Unauthorized"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());

  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
  }
  CHECK(errs.empty());

  if (root.contains("errors") && root["errors"].is_array())
  {
    STRCMP_EQUAL("Unauthorised access", root["errors"][0]["message"].get<std::string>().c_str());
  }
  else
  {
    CHECK(false);
  }

  //std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

TEST(RestClientSecureAccessor, AnswerConnectionPutJsonAuthProcessAccessor)
{
  RestClient::init();
  RestClient::Connection conn(SERVER_URL);
  //conn.SetCAInfoFilePath("certfile");

  std::string user = "rod";
  std::string password = "pa$$w0rd";
  std::string apikey = "213019FF46E0577C6BECEC9966F0426C";

  NonceGenerator ng;
  std::string sharedsecret = ng.secretKeyGenerate(password, apikey);
  std::string nonce = ng.generate();
  std::string method = "PUT";
  std::string resurl = "/persons";
  std::string currenttime = DateTimeStamp::currentDateTime();
  std::string tohash;
  tohash = user + "+" + method + "+" + resurl + "+" + currenttime + "+" + nonce;
  std::cout << "PostJson To Hash: " << tohash << "\n";
  HMACGenerator hmacGen;
  hmacGen.generate(sharedsecret, tohash);
  std::string genB64 = Base64::base64_encode(hmacGen.getSignatureBytes(), hmacGen.getSignatureSize());
  std::string authHead;
  authHead = user + ":" + nonce + ":" + genB64;
  conn.AppendHeader("Authorization", "hmac " + authHead);
  conn.AppendHeader("Date", currenttime);
  conn.AppendHeader("Accept", "application/json");
  conn.AppendHeader("Content-Type", "application/json");

  //conn.AppendHeader("Expect", "100-Continue");
  json person;
  person[PA.F.ID] = "300";
  person[PA.F.Name] = "Sam Cash";
  RestClient::Response rx = conn.put("/persons", person.dump());
  std::stringstream heads;
  for (auto h : rx.headers)
  {
    heads << " [" << h.first << "] " << h.second;
  }
  //std::cerr << heads.str() << "\n[" << rx.body << "]\n";
  CHECK(!rx.headers["HTTP/1.1 200 OK"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 200 OK"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());
  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
    std::cerr << errs << "\n";
  }
  CHECK(errs.empty());

  if (root.contains(PA.F.Name) && root[PA.F.Name].is_string())
  {
    STRCMP_EQUAL("Sam Cash", root[PA.F.Name].get<std::string>().c_str());
  }
  else
  {
    CHECK(false);
  }
}

TEST(RestClientSecureAccessor, AnswerConnectionPutInvlaidJsonAuthProcessAccessor)
{
  RestClient::init();
  RestClient::Connection conn(SERVER_URL);
  //conn.SetCAInfoFilePath("certfile");

  std::string user = "rod";
  std::string password = "pa$$w0rd";
  std::string apikey = "213019FF46E0577C6BECEC9966F0426C";

  NonceGenerator ng;
  std::string sharedsecret = ng.secretKeyGenerate(password, apikey);
  std::string nonce = ng.generate();
  std::string method = "PUT";
  std::string resurl = "/persons";
  std::string currenttime = DateTimeStamp::currentDateTime();
  std::string tohash;
  tohash = user + "+" + method + "+" + resurl + "+" + currenttime + "+" + nonce;
  std::cout << "PostJson To Hash: " << tohash << "\n";
  HMACGenerator hmacGen;
  hmacGen.generate(sharedsecret, tohash);
  std::string genB64 = Base64::base64_encode(hmacGen.getSignatureBytes(), hmacGen.getSignatureSize());
  std::string authHead;
  authHead = user + ":" + nonce + ":" + genB64;
  conn.AppendHeader("Authorization", "hmac " + authHead);
  conn.AppendHeader("Date", currenttime);
  conn.AppendHeader("Accept", "application/json");
  conn.AppendHeader("Content-Type", "application/json");

  RestClient::Response rx = conn.put("/persons", "{oo\":\"bla\"}"); // bad json parsed
  CHECK(!rx.headers["HTTP/1.1 406 Not Acceptable"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 406 Not Acceptable"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());
  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
  }
  CHECK(errs.empty());

  if (root.contains("errors") && root["errors"].is_array())
  {
    STRCMP_EQUAL("101 [json.exception.parse_error.101] parse error at line 1, column 2: syntax error while parsing object key - invalid literal; last read: '{o'; expected string literal", root["errors"][0]["message"].get<std::string>().c_str());
  }
  else
  {
    CHECK(false);
  }

  //std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

TEST(RestClientSecureAccessor, AnswerConnectionDeleteJsonAuthProcessAccessor)
{
  RestClient::init();
  RestClient::Connection conn(SERVER_URL);
  //conn.SetCAInfoFilePath("certfile");

  std::string user = "rod";
  std::string password = "pa$$w0rd";
  std::string apikey = "213019FF46E0577C6BECEC9966F0426C";

  NonceGenerator ng;
  std::string sharedsecret = ng.secretKeyGenerate(password, apikey);
  std::string nonce = ng.generate();
  std::string method = "DELETE";
  std::string resurl = "/persons";
  std::string currenttime = DateTimeStamp::currentDateTime();
  std::string tohash;
  tohash = user + "+" + method + "+" + resurl + "+" + currenttime + "+" + nonce;
  std::cout << "PostJson To Hash: " << tohash << "\n";
  HMACGenerator hmacGen;
  hmacGen.generate(sharedsecret, tohash);
  std::string genB64 = Base64::base64_encode(hmacGen.getSignatureBytes(), hmacGen.getSignatureSize());
  std::string authHead;
  authHead = user + ":" + nonce + ":" + genB64;
  conn.AppendHeader("Authorization", "hmac " + authHead);
  conn.AppendHeader("Date", currenttime);
  conn.AppendHeader("Accept", "application/json");
  conn.AppendHeader("Content-Type", "application/json");

  RestClient::Response rx = conn.del("/persons");
  CHECK(!rx.headers["HTTP/1.1 406 Not Acceptable"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 406 Not Acceptable"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());
  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
  }
  CHECK(errs.empty());

  if (root.contains("errors") && root["errors"].is_array())
  {
    STRCMP_EQUAL("Not accepted: DELETE", root["errors"][0]["message"].get<std::string>().c_str());
  }
  else
  {
    CHECK(false);
  }

  //std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

TEST(RestClientSecureAccessor, RestGetBasicUserOnHttps)
{
  // initialize RestClient
  RestClient::init();
  // get a connection object
  RestClient::Connection conn(SERVER_URL);
  //conn.SetCAInfoFilePath("certfile");
  // configure basic auth
  std::string user("rod:pa$$w0rd");
  std::string user_b64 = Base64::string_to_base64(user.c_str(), user.length());
  conn.AppendHeader("Authorization", "Basic " + user_b64);

  std::stringstream sendheads;
  for (auto h : conn.GetHeaders())
  {
    if (h.first.compare("Date") == 0)
    {
    }
    else
    {
      sendheads << " [" << h.first << "] " << h.second;
    }
  }
  STRCMP_EQUAL(" [Authorization] Basic cm9kOnBhJCR3MHJk", sendheads.str().c_str());

  RestClient::Response rx = conn.get("/persons");

  CHECK(!rx.headers["HTTP/1.1 401 Unauthorized"].empty());
  CHECK(!rx.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx.headers["HTTP/1.1 401 Unauthorized"].c_str());
  STRCMP_EQUAL("application/json", rx.headers["Content-Type"].c_str());
  std::string errs{};
  json root{};
  try
  {
    root = json::parse(rx.body);
  }
  catch (json::exception &e)
  {
    errs += e.what();
  }
  CHECK(errs.empty());

  if (root.contains("errors") && root["errors"].is_array())
  {
    STRCMP_EQUAL("Unauthorised access", root["errors"][0]["message"].get<std::string>().c_str());
  }
  else
  {
    CHECK(false);
  }

  conn.AppendHeader("Content-Type", "application/x-www-form-urlencoded");
  RestClient::Response rx2 = conn.post("/namepost", "name=\"Barry\"");
  // std::stringstream heads;
  // for (auto h : rx2.headers)
  // {
  //   heads << " [" << h.first << "] " << h.second;
  // }
  // std::cerr << heads.str() << "\n[" << rx.body << "]\n";
  CHECK(!rx2.headers["HTTP/1.1 200 OK"].empty());
  CHECK(!rx2.headers["Content-Type"].empty());
  STRCMP_EQUAL("present", rx2.headers["HTTP/1.1 200 OK"].c_str());
  STRCMP_EQUAL("application/json", rx2.headers["Content-Type"].c_str());
  STRCMP_EQUAL("<html><body><h1>Welcome, \"Barry\"!</center></h1></body></html>", rx2.body.c_str());
  //std::this_thread::sleep_for(std::chrono::milliseconds(100));
}
