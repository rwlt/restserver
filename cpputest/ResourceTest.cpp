/*
 * ResourceTest.cpp
 *
 *  Created on: 5/06/2017
 *      Author: rodney
 */

#include "RestServer.h"
#include "hashalgs/HMACGenerator.h"
#include "hashalgs/NonceGenerator.h"
#include "hashalgs/DateTimeStamp.h"
#include "hashalgs/Base64.h"
#include "PersonalModel.h"
#include "nholmann/json.hpp"
#include <string.h>
#include <cstring>

using json = nlohmann::json;

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

using Module::PA;
using Module::PersonalModel;
//
//-------------------------------------------------------
// RestClientAccessor test group
//-------------------------------------------------------

TEST_GROUP(ResourceAccessor)
{

  RestServer restserver;
  PersonalModel pb{true};

  void setup()
  {
    auto p1 = pb.addNewPerson(100, "Brian Bary");
    auto p2 = pb.addNewPerson(200, "Jack Brian");
    auto p3 = pb.addNewPerson(300, "Carl Boll");
    auto p4 = pb.addNewPerson(400, "David Smith");

    restserver.get("/persons", [this](const HttpRequest &)
                   {
                     std::unique_ptr<HttpResponse> res = std::make_unique<HttpResponse>();
                     json root;
                     auto list = pb.doRead();
                     root[PA.T.People] = json::array({});
                     for (auto &p : list)
                     {
                       json person;
                       auto pID = std::get<0>(p->get(PA.F.ID));
                       auto pName = std::get<0>(p->get(PA.F.Name));
                       person[PA.F.ID] = pID;
                       person[PA.F.Name] = pName;
                       root[PA.T.People].push_back(person);
                     }
                     res->set_status(Rest::ResponseIdentity::OK);
                     res->set_body(root.dump());
                     return res;
                   });
  }

  void teardown()
  {
  }
};

TEST(ResourceAccessor, doResourceTimeStampAccessor)
{

  DateTimeStamp uts("Aug 30 17 11:51:20 AM");
  CHECK(false == uts.acceptedTimeStamp(12));
  STRCMP_EQUAL("Aug301711:51:20", uts.getTimeStamp().c_str());
  DateTimeStamp uts4("Aug 30 2017");
  CHECK(false == uts4.acceptedTimeStamp(12));
  STRCMP_EQUAL("Jan012000:00:00", uts4.getTimeStamp().c_str());

  DateTimeStamp uts2(DateTimeStamp::currentDateTime());
  CHECK(true == uts2.acceptedTimeStamp(12));

  DateTimeStamp uts3("Aug301721:51:20 PM");
  CHECK(false == uts3.acceptedTimeStamp(12));
  STRCMP_EQUAL("Aug301721:51:20", uts3.getTimeStamp().c_str());

  NonceGenerator ng;

  std::string user = "user2";
  std::string password = "user2pa$$";
  std::string sharesalt = ng.generate();

  std::string sharesecret = ng.secretKeyGenerate(password, sharesalt);
  std::string sharesecret2 = ng.secretKeyGenerate(password, sharesalt);

  std::cout << "Users server info:\n";
  std::cout << "User:                                   " << user << "\n";
  std::cout << "Users password:                         " << password << "\n";
  std::cout << "Salt/APIKEY                             " << sharesalt << "\n";
  std::cout << "Secret:                                 " << sharesecret << "\n";
  std::cout << "The users password and salt is what creates the secret for HMAC digest\n";
  std::cout << "Secret2 recalculated as mentioned above:" << sharesecret2 << "\n\n";

  std::string method = "GET";
  std::string resurl = "/persons";
  std::string currenttime = DateTimeStamp::currentDateTime();
  std::string tohash;
  std::string nonce = ng.generate();
  tohash = user + "+" + method + "+" + resurl + "+" + currenttime + "+" + nonce;
  std::cout << "NonceGenerator nonce: " << nonce << "\n";
  std::cout << "To Hash: " << tohash << "\n";

  // Client to API uses sharesecret and generates authorization token
  HMACGenerator hmacGen;
  hmacGen.generate(sharesecret, tohash);

  std::cout << "Authorize hash generated: " << hmacGen.getPrintHash() << "\n";
  std::string genB64 = Base64::base64_encode(hmacGen.getSignatureBytes(), hmacGen.getSignatureSize());
  // Plus needs to send headers in request (idea here is the internal modules in an app communicate
  // for the user logged in)
  std::string authHead;
  authHead = user + ":" + nonce + ":" + genB64;
  std::cout << "Authorize " << authHead << "\n";
  std::cout << "Request date: " << currenttime << "\n";

  // Test the base64 encode/decode so it can be sent to RestAPI
  std::string auth_b64 = Base64::string_to_base64(authHead.c_str(), authHead.length());
  std::string auth_b64decode = Base64::string_from_base64(auth_b64.c_str());
  STRCMP_EQUAL(auth_b64decode.c_str(), authHead.c_str());

  // The RestAPI server receives the signature -
  // It finds sharesecert from database for user name.
  hmacGen.setSignatureFromB64(genB64); // set HMAC signature from the received signature from Rest Request
  int result = hmacGen.verify(sharesecret, tohash);
  LONGS_EQUAL(1, result); // success verified is result = 1 It is a valid to check nonce and timestamp

  // Test the verify on a different header values of user to the sent signaute of request
  std::string tohash2;
  tohash2 = user + "grp+" + method + "+" + resurl + "+" + currenttime + "+" + nonce;
  int result2 = hmacGen.verify(sharesecret, tohash2);
  LONGS_EQUAL(0, result2); // verified it is a changed signature so not signature verify
}

TEST(ResourceAccessor, doRouteHandler)
{
  CHECK_THROWS(std::exception,
               {
                 restserver.get("/persons/{ss", [this](const HttpRequest &)
                                { return std::make_unique<HttpResponse>(); });
               });
  CHECK_THROWS(std::exception, { restserver.get("/persons{ss}", [this](const HttpRequest &)
                                                { return std::make_unique<HttpResponse>(); }); });
  CHECK_THROWS(std::exception, { restserver.get("/persons/{ss}/detail", [this](const HttpRequest &)
                                                { return std::make_unique<HttpResponse>(); }); });
  CHECK_THROWS(std::exception, { restserver.get("/persons/{_ID}", [this](const HttpRequest &)
                                                { return std::make_unique<HttpResponse>(); }); });
}

TEST(ResourceAccessor, doRouteHandlerMatch)
{
  try
  {
    restserver.get("/staff/person/mate/{P_ID}/{Name}/{Title}", [this](const HttpRequest &)
                   { return std::make_unique<HttpResponse>(); });
    std::cerr << "doRouteHandlerMatch#####################\n";
    MatchRoute route = restserver.route_handler().match({"GET", "/staff/person/mate/3/Fred/Mr"});
    auto params = route.second;
    STRCMP_EQUAL("3", params["P_ID"].c_str());
  }
  catch (std::exception &e)
  {
    std::cerr << "Except: " << e.what() << "\n";
  }
}
