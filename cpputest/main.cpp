#include <iostream>
#include "CppUTest/CommandLineTestRunner.h"

int main(int argc, char **argv)
{
  /* initialize random seed: */
  srand(time(NULL));

  std::cout << "This is " << argv[0] << std::endl;
  int ret = CommandLineTestRunner::RunAllTests(argc, argv);
  return ret;
}
