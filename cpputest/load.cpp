#include "PersonalModel.h"
#include "nholmann/json.hpp"
#include "restclient-cpp/restclient.h"
#include "restclient-cpp/connection.h"
#include <random>
#include <thread>

constexpr const char *SERVER_URL = "http://localhost:8888";

using json = nlohmann::json;
using Module::PA;
//using Module::PersonalModel;

auto names = std::vector<std::string>{"Gary", "Barry", "Carl", "John", "Brian", "David", "Boll", "Carry", "Sue", "Delia"};
std::default_random_engine generator;
std::uniform_int_distribution<int> distribution(0, 9);

json peopleCreater(int startID, int amt)
{
  json peopleL1;
  peopleL1[PA.T.People] = json::array({});

  // Fake names
  auto itemran = std::bind(distribution, generator);

  for (int i = startID; i < (startID + amt); i++)
  {
    auto r1 = itemran();
    auto r2 = itemran();
    auto name = names[r1] + " " + names[r2];
    json person;
    person[PA.F.ID] = i;
    person[PA.F.Name] = name;
    peopleL1[PA.T.People].push_back(person);
  }
  return peopleL1;
};

int main(int, char **)
{
  RestClient::init();
  RestClient::Connection conn1(SERVER_URL);
  RestClient::Connection conn2(SERVER_URL);
  RestClient::Connection conn3(SERVER_URL);
  RestClient::Connection conn4(SERVER_URL);
  bool doPrint{true};

  while (true)
  {
    try
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
      std::cerr << "Next loop\n";

      json peopleL1 = peopleCreater(1000, 7500);
      json peopleL2 = peopleCreater(9000, 7500);
      json peopleL3 = peopleCreater(18000, 7500);
      json peopleL4 = peopleCreater(26000, 7500);

      // Create threads with each a put to SERVER_URL
      std::thread t1([peopleL1, &conn1, doPrint]() {
        RestClient::Response rx = conn1.put("/persons", peopleL1.dump());
        std::string errs{};
        json root{};
        if (doPrint) {
          std::stringstream heads;
          for (auto h : rx.headers)
          {
            heads << " [" << h.first << "] " << h.second;
          }
          std::cerr << "t1 rx: " << heads.str() << "\n";//[" << rx.body << "]\n";
        }
        try
        {
          root = json::parse(rx.body);
        }
        catch (json::exception &e)
        {
          errs += e.what();
          std::cerr << errs << "\n";
        }
      });

      std::thread t2([peopleL2, &conn2, doPrint]() {
        RestClient::Response rx = conn2.put("/persons", peopleL2.dump());
        std::string errs{};
        if (doPrint) {
          std::stringstream heads;
          for (auto h : rx.headers)
          {
            heads << " [" << h.first << "] " << h.second;
          }
          std::cerr << "t2 rx: " << heads.str() << "\n";//[" << rx.body << "]\n";
        }
        json root{};
        try
        {
          root = json::parse(rx.body);
        }
        catch (json::exception &e)
        {
          errs += e.what();
          std::cerr << errs << "\n";
        }
      });
      std::thread t3([peopleL3, &conn3, doPrint]() {
        RestClient::Response rx = conn3.put("/persons", peopleL3.dump());
        std::string errs{};
        if (doPrint) {
          std::stringstream heads;
          for (auto h : rx.headers)
          {
            heads << " [" << h.first << "] " << h.second;
          }
          std::cerr << "t3 rx: " << heads.str() << "\n";//[" << rx.body << "]\n";
        }
        json root{};
        try
        {
          root = json::parse(rx.body);
        }
        catch (json::exception &e)
        {
          errs += e.what();
          std::cerr << errs << "\n";
        }
      });

      RestClient::Response rx = conn4.put("/persons", peopleL4.dump());

      t1.join();
      t2.join();
      t3.join();

      std::string errs{};
        if (doPrint) {
          std::stringstream heads;
          for (auto h : rx.headers)
          {
            heads << " [" << h.first << "] " << h.second;
          }
          std::cerr << "main rx: " << heads.str() << "\n";//[" << rx.body << "]\n";
        }
      json root{};
      try
      {
        root = json::parse(rx.body);
      }
      catch (json::exception &e)
      {
        errs += e.what();
        std::cerr << errs << "\n";
      }
    }
    catch (std::exception &e)
    {
      std::cerr << e.what() << "\n";
      break;
    }
    catch (...)
    {
      std::cerr << "excepted\n";
    }
  }
  std::cerr << "out\n";

  return 0;
}
