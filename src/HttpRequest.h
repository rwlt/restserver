#ifndef SRC_RESTHTTPREQUEST_H
#define SRC_RESTHTTPREQUEST_H

#include "Request.h"
#include <string>
#include <vector>

using Rest::Request;

namespace Rest
{
  constexpr const char *HMAC_TOKEN = "hmac ";

  class HttpRequest : public Request
  {
    HeaderList m_headers{};
    std::string m_body{};
    Parameters m_params{};

  public:
    HttpRequest();
    virtual ~HttpRequest();

    virtual std::string response_id() override
    {
      return "HttpResponse";
    };

    inline virtual void set_body(const std::string &body) override
    {
      m_body = body;
    };

    inline virtual std::string body() const override
    {
      return m_body;
    };

    inline const Parameters &params() const override
    {
      return m_params;
    };

    inline virtual void add_header(const std::string &name, const std::string &value) override
    {
      //std::cerr << "header key:" << name << " " << value << "\n";
      m_headers.push_back({name, value});
    }

    inline virtual HeaderList headers() const override
    {
      return m_headers;
    }

    // inline virtual void add_header(const char *, const char *) override {
    //   throw std::runtime_error("use add_header(const std::string &name, const std::string &value)");
    //   //m_p_headers.push_back({name, value});
    // };

    // virtual HeaderPList p_headers() const override 
    // {
    //   throw std::runtime_error("use headers()");
    //   //return m_identity_const_headers;
    // }

  };

}

#endif /* SRC_RESTHTTPREQUEST_H */