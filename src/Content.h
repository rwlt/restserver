#ifndef SRC_RESTREQUESTRESPONSCONTENT_H
#define SRC_RESTREQUESTRESPONSCONTENT_H

#include <string>
#include <iostream>

namespace Rest
{

  class Content
  {
  public:
    virtual ~Content(){};
    virtual void set_body(const std::string &body) = 0;
    virtual std::string body() const = 0;
  };

}

#endif /* SRC_RESTREQUESTRESPONSCONTENT_H */