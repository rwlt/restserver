//
// Created by rodney on 17/04/2021.
//

#ifndef SRC_RESTROUTE_H
#define SRC_RESTROUTE_H

#include "Request.h"
#include "Response.h"
#include <memory>
#include <functional>

using Rest::Request;
using Rest::Response;

namespace Rest
{
  //std::unique_ptr<Response> default_route_func(const Request &req, std::unique_ptr<Response> res);
  //Response& default_route_func(const Request &req, Response& res);

  class Route
  {
  public:
    //using RouteCall = std::function<std::unique_ptr<Response>(const Request &req, std::unique_ptr<Response> res)>;
//    using RouteCall = std::function<Response&(const Request &req, Response& res)>;

  //  Route() : m_routeFunc(default_route_func) {}
    virtual ~Route(){};

    virtual std::string request_id() = 0;

  //   void register_route_callback(RouteCall callback)
  //   {
  //     m_routeFunc = callback;
  //   };
  //   //std::unique_ptr<Response> do_route(const Request &req, std::unique_ptr<Response> res)
  //   Response& do_route(const Request &req, Response& res)
  //   {
  //     return m_routeFunc(req, res);
  //   };

  // private:
  //   RouteCall m_routeFunc;
  };

}

#endif /* SRC_RESTROUTE_H */