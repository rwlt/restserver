#ifndef SRC_RESTREQUESTRESPONSHEADERS_H
#define SRC_RESTREQUESTRESPONSHEADERS_H

#include <string>
#include <vector>

namespace Rest
{
  constexpr const char *CONTENT_TYPE = "Content-Type";
  constexpr const char *APPLICATION_JSON = "application/json";

  using HeaderList = std::vector<std::pair<std::string, std::string>>;
  //using HeaderPList = std::vector<std::pair<const char *, const char *>>;
  class Headers
  {
  public:
    virtual ~Headers(){};
    virtual void add_header(const std::string& name, const std::string& value) = 0;
    virtual HeaderList headers() const = 0;

    // For micro responses header requirement
    //virtual void add_header(const char * name, const char * value) = 0;
    //virtual HeaderPList p_headers() const = 0;

    //virtual HeaderIdentityList identity_headers() const = 0;
    //virtual void add_header(const HeaderIdentity &name, const HeaderValueIdentity &value) = 0;
  };

}

#endif /* SRC_RESTREQUESTRESPONSHEADERS_H */

  // enum HeaderIdentity
  // {
  //   ContentType = 0 // uses define MHD_HTTP_HEADER_CONTENT_TYPE
  // };
  // enum HeaderValueIdentity
  // {
  //   ApplicationJson = 0 // uses define MHD_HTTP_HEADER_CONTENT_TYPE
  // };
  // const header value for reponses in microhttpd
  //using HeaderIdentityList = std::vector<std::pair<HeaderIdentity, HeaderValueIdentity>>;
  // for (auto h : con_info.identity_headers())
  // {
  //   if (h.first == Rest::HeaderIdentity::ContentType && h.second == Rest::HeaderValueIdentity::ApplicationJson)
  //   {
  //     ret = MHD_add_response_header(response, Rest::CONTENT_TYPE, Rest::APPLICATION_JSON);
  //     if (!ret)
  //     {
  //       free(me);
  //       MHD_destroy_response(response);
  //       return MHD_NO;
  //     }
  //   }
  // }
