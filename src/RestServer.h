/*
 * RestServer.h
 *
 *  Created on: 26/08/2017
 *      Author: rodney
 */

#ifndef SRC_APP_RESTSERVER_H_
#define SRC_APP_RESTSERVER_H_


#ifdef __cplusplus
extern "C"
{
#endif
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/time.h>
  long get_file_size(const char *filename);
  char *load_file(const char *filename);
#ifdef __cplusplus
}
#endif
#include <microhttpd.h>
#include "RouteHandler.h"
#include "HttpRoute.h"
//#include "NonceHistory.h"
#include <sstream>
#include <map>
#include <cstring>

//#include "RestRequest.h"

using Rest::HttpRequest;
using Rest::HttpResponse;
using Rest::HttpRoute;
using Rest::Parameters;
using Rest::RouteHandler;
using MatchRoute = std::pair<std::unique_ptr<HttpRoute>, Parameters>;

constexpr int PORT = 8888;
constexpr int MAXNAMESIZE = 20;

#ifdef __cplusplus
extern "C"
{
#endif

  int ask_for_authentication(struct MHD_Connection *connection,
                             const char *realm);
  int is_authenticated(struct MHD_Connection *connection,
                       const char *username, const char *password);

  // Static libmicrohttp specific methods
  int request_headers(void *cls, enum MHD_ValueKind kind, const char *key, const char *value);

  // static int iterate_post(void *coninfo_cls, enum MHD_ValueKind kind, const char *key, const char *filename,
  //                         const char *content_type, const char *transfer_encoding, const char *data, uint64_t off, size_t size);

  // answer with post to process
  int rest_connection(void *cls, struct MHD_Connection *connection, const char *url, const char *method,
                 const char *version, const char *upload_data, size_t *upload_data_size, void **con_cls);

  int uploadBody(const char *method, const char *upload_data, size_t *upload_data_size, std::ostringstream *con_body);

  // Send a page
  int send_page(struct MHD_Connection *connection, HttpResponse *con_info);

  int answer_with_authentication(void *cls, struct MHD_Connection *connection, const char *url, const char *method,
                                 const char *version, const char *upload_data, size_t *upload_data_size, void **con_cls);
#ifdef __cplusplus
}
#endif

class RestServer
{
  RouteHandler m_route_handler;
  //NonceHistory m_nonceHistory;
  char *key_pem;
  char *cert_pem;
  struct MHD_Daemon *daemon;

public:
  RestServer();
  virtual ~RestServer();

  void get(const std::string &route, HttpRoute::RouteCall);
  void put(const std::string &route, HttpRoute::RouteCall);

  inline const RouteHandler &route_handler() const { return m_route_handler; };

  using UserKeyCallReturn = std::tuple<bool, std::string>;
  UserKeyCallReturn read_user_key(std::string username);

  int run();
};

#endif /* SRC_APP_RESTSERVER_H_ */
