#ifndef SRC_RESTRESPONSE_H
#define SRC_RESTRESPONSE_H

#include "Content.h"
#include "Headers.h"
#include <string>

namespace Rest
{
  /// Identity of response to return

  enum ResponseIdentity
  {
    OK = 0,          ///< OK response
    AuthFail = 1,    ///< Authorization wrong
    NotAccepted = 2, ///< Content format in error
    FileNotFound = 3,
    ServerError = 4
  };

  constexpr const char *HMAC_TOKEN3 = "hmac ";

  //template <typename IdentifierType = std::string>
  class Response : public Content, public Headers
  {

  public:
    virtual ~Response(){};

    virtual void set_status(ResponseIdentity responseID) = 0;
    virtual ResponseIdentity status() const = 0;
  };

}

#endif /* SRC_RESTRESPONSE_H */