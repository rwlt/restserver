#ifndef SRC_RESTHTTPRESPONSE_H
#define SRC_RESTHTTPRESPONSE_H

#include "Response.h"
#include <string>

using Rest::Response;
//using Rest::ResponseIdentity;

namespace Rest
{
  class HttpResponse : public Response
  {
    HeaderList m_headers{};
    std::string m_body{};
    ResponseIdentity m_responseID{ResponseIdentity::OK};

  public:
    virtual ~HttpResponse();

    HttpResponse() = default;
    HttpResponse(HttpResponse &&) = default;
    HttpResponse(const HttpResponse &) = default;

    inline virtual void set_body(const std::string &body) override
    {
      m_body = body;
    };

    inline virtual std::string body() const override
    {
      return m_body;
    };

    inline virtual void set_status(ResponseIdentity responseID) override
    {
      m_responseID = responseID;
    };
    inline virtual ResponseIdentity status() const override
    {
      return m_responseID;
    };

    inline virtual void add_header(const std::string &name, const std::string &value) override
    {
      //throw std::runtime_error("use add_header(const char * name, const char * value)");
      m_headers.push_back({name, value});
    }

    inline virtual HeaderList headers() const override
    {
      //throw std::runtime_error("use p_headers()");
      return m_headers;
    }

    // inline virtual void add_header(const char *name, const char *value) override {
    //   //throw std::runtime_error("use add_header(const std::string &name, const std::string &value)");
    //   m_p_headers.push_back({name, value});
    // };

    // virtual HeaderPList p_headers() const override 
    // {
    //   //throw std::runtime_error("use headers()");
    //   return m_p_headers;
    // }

  };

}

#endif /* SRC_RESTHTTPRESPONSE_H */

    // inline virtual void add_header(const HeaderIdentity &, const HeaderValueIdentity &) override
    // {
    //   throw std::runtime_error("use add_header(const char * name, const char * value)");
    //   // if (name == Rest::HeaderIdentity::ContentType) {
    //   //   std::cerr << "header key:" << Rest::CONTENT_TYPE << " ";
    //   // }
    //   // if (value == Rest::HeaderValueIdentity::ApplicationJson) {
    //   //   std::cerr << "header value:" << Rest::APPLICATION_JSON << " ";
    //   // }
    //   // std::cerr << "\n";

    //   // m_identity_headers.push_back({name, value});
    // }

    // inline virtual HeaderIdentityList identity_headers() const override
    // {
    //   throw std::runtime_error("use identity_const_headers()");
    //   //return m_identity_headers;
    // }
