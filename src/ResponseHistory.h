/*
 * ResponseHistory.h
 *
 *  Created on: 12/05/2021
 *      Author: rodney
 */

#ifndef SRC_RESTRESPONSEHISTORY_H_
#define SRC_RESTRESPONSEHISTORY_H_

#include "hashalgs/DateTimeStamp.h"
#include "Response.h"
#include <stdexcept>
#include <string>
#include <map>
#include <mutex>

namespace Rest
{

  using TimestampResponse = std::map<std::string, std::vector<Response>>;
  typedef std::pair<std::string, DateTimeStamp> MapNoncePair;

  class ResponseHistory
  {

    MapNonce m_nonces;
    mutable std::mutex m_Mutex{};
    static const int secondexpiry;

  public:
    NonceHistory()
    {
    }
    virtual ~NonceHistory()
    {
    }

    void addNonce(std::string nonce, DateTimeStamp timestamp);

    inline bool findNonce(std::string nonce)
    {
      return (m_nonces.count(nonce) > 0);
    }

    std::size_t size()
    {
      return m_nonces.size();
    }
  };

}
#endif /* SRC_RESTRESPONSEHISTORY_H_ */
