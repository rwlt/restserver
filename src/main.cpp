//============================================================================
#include "RestServer.h"
#include "storage/storage.h"
#include "PersonalModel.h"
#include "nholmann/json.hpp"
#include <random>

using json = nlohmann::json;
using Module::PA;
using Module::PersonalModel;

//#include <stdio.h>

int main(int /*argc*/, char ** /*argv*/)
{
  RestServer restserver;
  int ret = 0;

  Module::PersonalModel pb{true};

  // Fake names
  auto names = std::vector<std::string>{"Gary", "Barry", "Carl", "John", "Brian", "David", "Boll", "Carry", "Sue", "Delia"};
  std::default_random_engine generator;
  std::uniform_int_distribution<int> distribution(0, 9);
  auto itemran = std::bind(distribution, generator);

  pb.beginTransaction();
  for (int i = 0; i < 10; i++)
  {
    auto r1 = itemran();
    auto r2 = itemran();
    auto name = names[r1] + " " + names[r2];
    pb.addNewPerson(i, name);
  }
  pb.endTransaction();

  try
  {
    restserver.get("/persons", [&pb](const HttpRequest &req)
                   {
                     std::unique_ptr<HttpResponse> res2 = std::make_unique<HttpResponse>();
                     json root;
                     auto params = req.params();
                     params["sd"] = "mine";
                     params["sd"] = "me";
                     auto list = pb.doRead();
                     root[PA.T.People] = json::array({});
                     for (auto &p : list)
                     {
                       json person;
                       auto pID = std::get<0>(p->get(PA.F.ID));
                       auto pName = std::get<0>(p->get(PA.F.Name));
                       person[PA.F.ID] = pID;
                       person[PA.F.Name] = pName;
                       root[PA.T.People].push_back(person);
                     }
                     res2->set_status(Rest::ResponseIdentity::OK);
                     res2->add_header("Content-Type", "application/json");
                     res2->set_body(root.dump());
                     return res2;
                   });

    restserver.put("/persons", [&pb](const HttpRequest &req)
                   {
                     json root;
                     json errs;
                     errs["errors"] = json::array({}); // accumalate errs and set Request object then
                     std::unique_ptr<HttpResponse> res2 = std::make_unique<HttpResponse>();

                     // std::cerr << "HEADERS:\n";
                     // for (auto h: req.headers()) {
                     //   std::cerr << h.first << " " << h.second << "\n";
                     // }
                     //std::cerr << "BODY:" << req.body();
                     std::vector<std::string> errList;
                     try
                     {
                       root = json::parse(req.body());
                     }
                     catch (json::exception &e)
                     {
                       errList.push_back(std::to_string(e.id) + " " + e.what());
                     }

                     res2->add_header("Content-Type", "application/json");
                     if (errList.size() > 0)
                     {
                       res2->set_status(Rest::ResponseIdentity::NotAccepted);
                       for (auto err : errList)
                       {
                         json msg;
                         msg["message"] = err;
                         errs["errors"].push_back(msg);
                       }
                       res2->set_body(root.dump());
                     }
                     else
                     {
                       //  Just sending back what was sent to work
                       std::string document = root.dump();
                       res2->set_status(Rest::ResponseIdentity::OK);
                       res2->set_body(root.dump());
                     }
                     return res2;
                   });

    ret =restserver.run();
  }
  catch (std::exception &e)
  {
    std::cerr << "Except: " << e.what() << "\n";
  }
  std::cout << "Done.\n";
  return ret;
}
