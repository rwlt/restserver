/*
 * PersonalModel.cpp
 *
 *  Created on: 20/07/2017
 *      Author: rodney
 */

#include "PersonalModel.h"
// #include "ModuleException.h"
// #include "PersonValidate.h"
// #include "Entity.h"

namespace Module
{

  // //
  // ///////////////////////////////////////////////////////////////////////////////
  // // public function
  // ///////////////////////////////////////////////////////////////////////////////
  // //
  // std::vector<Validate::ValidationError> PersonalModel::validatePerson(const Module::Person &person) {
  // 	Validate::PersonValidate person_validate(person);
  // 	std::vector<Validate::ValidationError> validation_errors = person_validate.Validate();
  // 	return validation_errors;
  // }
  //
  ///////////////////////////////////////////////////////////////////////////////
  // public function
  ///////////////////////////////////////////////////////////////////////////////
  //
  PEntity PersonalModel::addNewPerson(int id, const std::string &name)
  {
    // if (!loggedUser()) {
    // 	throw(ModuleException("PersonalModule addNewPerson no logged in user."));
    // }

    ///	auto errors = validatePerson(addPerson);
    // if (!errors.empty()) {
    // 	std::string message = validationMessage(errors);
    // 	throw(ModuleException("PersonalModule addNewPerson " + message));
    // }
    auto e = m_people.entity();
    e->set(PA.F.ID, std::to_string(id));
    e->set(PA.F.Name, name);
    e->save();
    return e;
    // if (m_repoPerson.isTrackingState()) {
    // 	// find if in tracked remove state
    // 	try {
    // 	auto key = m_repoPerson.addItem(addPerson);
    // 	addedPerson = m_repoPerson.getItem(key);
    // 	} catch (Repository::RepositoryException& re) {
    // 		throw(ModuleException("PersonalModel addNewPerson " + std::string(re.what())));
    // 	}
    // } else {
  }
  // for (auto func: this->m_broadcastPersonChange) {
  // 	func(addedPerson, false);
  // }
  //return addedPerson;

  //
  ///////////////////////////////////////////////////////////////////////////////
  // public function
  ///////////////////////////////////////////////////////////////////////////////
  //
  PEntity PersonalModel::changePerson(PEntity entity)
  {

    entity->save();
    // if (!loggedUser())
    // {
    //   throw(ModuleException("PersonalModule changePerson no logged in user."));
    // }
    ////  auto errors = validatePerson(changePerson);
    // if (!errors.empty())
    // {
    //   std::string message = validationMessage(errors);
    //   throw(ModuleException("PersonalModule changePerson " + message));
    // }

    // Module::Person changedPerson;
    // if (m_repoPerson.isTrackingState())
    // {
    //   m_repoPerson.updateItem(changePerson.getPersonID(), changePerson);
    //   changedPerson = m_repoPerson.getItem(changePerson.getPersonID());
    // }
    // else
    // {
    //   changedPerson = m_dbStorage.doUpdateItem<Person>(origPerson, changePerson);
    // }
    // for (auto func : this->m_broadcastPersonChange)
    // {
    //   func(changedPerson, false);
    // }
    return entity;
  }
  //
  ///////////////////////////////////////////////////////////////////////////////
  // public function
  ///////////////////////////////////////////////////////////////////////////////
  //
  // void PersonalModel::removePerson(const Module::Person &removePerson) {
  // 	Module::Person bcRemovePerson = removePerson;
  // 	if (!loggedUser()) {
  // 		throw(ModuleException("PersonalModule removePerson no logged in user."));
  // 	}
  //     if (m_repoPerson.isTrackingState()) {
  //     	m_repoPerson.deleteItem(removePerson.getPersonID());
  //     } else {
  //     	m_dbStorage.doRemoveItem<Person>(removePerson);
  //     }
  // 	for (auto func: this->m_broadcastPersonChange) {
  // 		func(bcRemovePerson, true);
  // 	}
  // }

  //
  /////////////////////////////////////////////////////////////////////////////
  // private function
  /////////////////////////////////////////////////////////////////////////////
  //
  // std::string PersonalModel::validationMessage(const std::vector<Validate::ValidationError> &errors) {
  // 	std::stringstream message;
  // 	for (auto error : errors) {
  // 		message << error.getFieldName() << " " << error.getErrorMessage() << "\n";
  // 	}
  // 	return message.str();
  // }

} /* namespace Module */
