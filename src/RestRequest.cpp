/*
 * RestRequest.cpp
 *
 *  Created on: 26/08/2017
 *      Author: rodney
 */

#include "RestRequest.h"
#include <cstring>
#include <sstream>
#include <iostream>
#include "hashalgs/HMACGenerator.h"


#ifdef DEBUG
#define D(x)
#else
#define D(x) x
#endif

const char* RestRequest::errorpageExpired = "<html><body>The request date is expired.</body></html>";
const char* RestRequest::errorpageRepeated = "<html><body>The request is a repeat to be right.</body></html>";
const char* RestRequest::errorpageUnknownUser = "<html><body>The request is unknown.</body></html>";
const char* RestRequest::errorpageRequestError = "<html><body>The request requires Authorization and Date headers.</body></html>";
const char* RestRequest::errorpage = "<html><body>This doesn't seem to be right.</body></html>";

bool RestRequest::is_valid_request ()
{
  bool valid;
  valid = false;
  valid = !(m_user.compare("") == 0 || m_date.compare("") == 0 ||
		    m_nonce.compare("") == 0 || m_hashRecB64.compare("") == 0);
  std::cerr << "RestRequest::is_valid_request\nuser:  " << m_user << "\ndate:  "  << m_date << "\nnonce: " << m_nonce << "\nhash:  " << m_hashRecB64 << "\nOK:    " << valid << "\n";
  return valid;
}


bool RestRequest::is_hmac_authorized (std::string secret)
{
  bool authenticated;
  authenticated = false;
  std::string tohash = m_user + "+" + connectiontype() + "+" + m_url + "+" + m_date + "+" + m_nonce;
  std::cout << "RestRequest::is_hmac_authorized: " << tohash << "\n";
  HMACGenerator hmacGen;
  hmacGen.setSignatureFromB64(m_hashRecB64);
  authenticated = hmacGen.verify(secret, tohash);
  return authenticated;
}

//
// Private function
//
std::vector<std::string> RestRequest::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}
//
// Private function
//
std::string RestRequest::connectiontype () {
	std::string verb;
	switch (m_restverb) {
	case RestVerb::Post:
		verb = "POST";
		break;
	case RestVerb::Get:
		verb = "GET";
		break;
	case RestVerb::Put:
		verb = "PUT";
		break;
	case RestVerb::Delete:
		verb = "DELETE";
		break;
	default:
		verb = "OTHER";
		break;
	}
	return verb;

}

