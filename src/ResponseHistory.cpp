#include "ResponseHistory.h"
#include <algorithm>
#include <iostream>

const int NonceHistory::secondexpiry = 15;

void NonceHistory::addNonce(std::string nonce, DateTimeStamp timestamp)
{
	if (m_nonces.count(nonce) != 0)
		return;
	if (timestamp.acceptedTimeStamp(secondexpiry)) {
		std::lock_guard<std::mutex> lock(m_Mutex);

		MapNoncePair pair = std::make_pair(nonce, timestamp);
		m_nonces.insert(pair);
        std::cout << "NonceHistory::addNonce Added nonce " << nonce << "\n";

		// Erase the expired ones to keep size down to minimum
		auto pm_it = m_nonces.begin();
		while(pm_it != m_nonces.end())
		{
		    if (pm_it->second.acceptedTimeStamp(NonceHistory::secondexpiry) == false)
		    {
		        std::cout << "NonceHistory::addNonce Erase nonce " << pm_it->first << "\n";
		        pm_it = m_nonces.erase(pm_it);
		    }
		    else
		    {
		        ++pm_it;
		    }
		}
	}
}

