//
// Created by rodney on 17/04/2021.
//

#ifndef SRC_RESTHTTPROUTE_H
#define SRC_RESTHTTPROUTE_H

#include "Route.h"
#include "HttpRequest.h"
#include "HttpResponse.h"
#include <memory>

using Rest::Route;

namespace Rest
{
  using PHttpResponse = std::unique_ptr<HttpResponse>;
  PHttpResponse default_http_route_func(const HttpRequest &req);

  class HttpRoute : public Route
  {

  public:
    using RouteCall = std::function<PHttpResponse(const HttpRequest &req)>;
    HttpRoute(): m_routeFunc(default_http_route_func) {};

    virtual ~HttpRoute();
    inline virtual std::string request_id() override
    {
      return "HttpRequest";
    };

    void register_route_callback(RouteCall callback)
    {
      m_routeFunc = callback;
    };
    PHttpResponse do_route(const HttpRequest &req)
    {
      return m_routeFunc(req);
    };

  private:
    RouteCall m_routeFunc;
  };

}

#endif /* SRC_RESTHTTPROUTE_H */