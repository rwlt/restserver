/*
 * RestServer.cpp
 *
 *  Created on: 26/08/2017
 *      Author: rodney
 */

#include "RestServer.h"
//#include "RestRequest.h"
// #include "hashalgs/NonceGenerator.h"
// #include "hashalgs/DateTimeStamp.h"
//#include "hashalgs/Base64.h"
#include "nholmann/json.hpp"
#include <thread>
#include <csignal>

using json = nlohmann::json;

#ifdef NDEBUG
#define D(x)
#else
#define D(x) x
#endif

namespace
{
  volatile std::sig_atomic_t gSignalStatus;
  void signal_handler(int signal)
  {
    std::cout << "signal " << signal << "\n";
    gSignalStatus = signal;
  }
}
//using mhd_pair = std::pair<std::ostringstream *, RestRequest *>;

// 	"<table entity=\"User\" name=\"User\" key=\"UserID\">\n"
// 	"        <field name=\"UserID\" type=\"integer\"/>\n"
// 	"        <field name=\"UserName\" type=\"string\"/>\n"
//  "        <field name=\"Salt\" type=\"string\"/>\n"
//  "        <field name=\"Secret\" type=\"string\"/>\n"
#ifdef __cplusplus
extern "C"
{
#endif

  int ask_for_authentication(struct MHD_Connection *connection, const char *realm)
  {
    int ret;
    struct MHD_Response *response;
    char *headervalue;
    const char *strbase = "Basic realm=";

    response = MHD_create_response_from_buffer(0, NULL,
                                               MHD_RESPMEM_PERSISTENT);
    if (!response)
      return MHD_NO;

    headervalue = (char *)malloc(strlen(strbase) + strlen(realm) + 1);
    if (!headervalue)
      return MHD_NO;

    strcpy(headervalue, strbase);
    strcat(headervalue, realm);

    ret = MHD_add_response_header(response, "WWW-Authenticate", headervalue);
    free(headervalue);
    if (!ret)
    {
      MHD_destroy_response(response);
      return MHD_NO;
    }

    ret = MHD_queue_response(connection, MHD_HTTP_UNAUTHORIZED, response);

    MHD_destroy_response(response);

    return ret;
  }

  int is_authenticated(struct MHD_Connection *connection,
                       const char *username, const char *password)
  {
    const char *headervalue;
    std::string expected_b64, expected;
    const char *strbase = "Basic ";
    int authenticated;

    headervalue =
        MHD_lookup_connection_value(connection, MHD_HEADER_KIND,
                                    "Authorization");
    if (NULL == headervalue)
      return 0;

    authenticated = 0;
    // Basic
    if (0 == strncmp(headervalue, strbase, strlen(strbase)))
    {

      std::stringstream ucat;
      ucat << username << ":" << password;
      expected_b64 = "";//Base64::string_to_base64(ucat.str().c_str(), ucat.str().length());

      authenticated =
          (strcmp(headervalue + strlen(strbase), expected_b64.c_str()) == 0);
    }
    return authenticated;
  }

  int request_headers(void *cls, enum MHD_ValueKind, const char *key, const char *value)
  {
    //RestRequest *con_info = (RestRequest *)cls;
    HttpRequest *con_info = (HttpRequest *)cls;
    con_info->add_header(std::string(key), std::string(value));
    // if (0 == strncmp(key, "Authorization", strlen("Authorization")))
    // {
    //   std::string keyValue(value);
    //   if (keyValue.size() >= 5 && (keyValue.substr(0, 5) == Rest::HMAC_TOKEN))
    //   {
    //     D(std::cout << "request_headers: " << keyValue.substr(5) << "\n";)
    //     con_info->setAuthorization(keyValue.substr(5));
    //   }
    // }
    // if (0 == strncmp(key, "Date", strlen("Date")))
    // {
    //   D(std::cout << "request_headers: " << value << "\n";)
    //   con_info->setDate(value);
    // }
    return MHD_YES;
  }

  //
  // Processing Post data
  //
  // int RestServer::iterate_post(void *coninfo_cls, enum MHD_ValueKind /*kind*/, const char *key, const char * /*filename*/,
  //                              const char * /*content_type*/, const char * /*transfer_encoding*/, const char *data, uint64_t /*off*/, size_t size)
  // {
  //   std::cout << "iterate_post " << key << " " << data << "\n";
  //   RestRequest *con_info = (RestRequest *)coninfo_cls;
  //   if (0 == strcmp(key, "name"))
  //   {
  //     if ((size > 0) && (size <= MAXNAMESIZE))
  //     {
  //       con_info->setResult("<html><body><h1>Welcome, " + std::string(data) + "!</center></h1></body></html>");
  //     }
  //     else
  //       con_info->setResult("");

  //     return MHD_NO;
  //   }
  //   return MHD_YES;
  // }

  int rest_connection(void *cls, struct MHD_Connection *connection,
                 const char *url, const char *method,
                 const char * /*version*/,
                 const char *upload_data,
                 size_t *upload_data_size,
                 void **con_cls)
  {

    if (NULL == *con_cls)
    {
      std::ostringstream *con_body = new std::ostringstream{std::ios::ate};
      if (con_body == NULL)
        return MHD_NO;
      *con_cls = (void *)con_body; // store pointer for result callback info
      return MHD_YES;
    }

    RestServer *rServer = (RestServer *)cls;

    std::ostringstream *con_body = (std::ostringstream *)*con_cls;

    if (*upload_data_size > 0) // process the upload request body
    {
      return uploadBody(method, upload_data, upload_data_size, con_body);
    }
    con_body->put('\0');

    json errs;
    errs["errors"] = json::array({}); // accumalate errs and set Request object then

    if (!con_body->good())
    {
      delete con_body;

      json msg;
      HttpResponse response;
      response.set_status(Rest::ResponseIdentity::NotAccepted);
      msg["message"] = "Error in request body";
      errs["errors"].push_back(msg);
      response.set_body(errs.dump());
      auto send_result = send_page(connection, &response);
      return send_result;
    }

    auto requestBody = con_body->str();
    auto bodySize = requestBody.size();
    delete con_body;

    // The request body is read.
    // RestRequest *con_info = new RestRequest(method,url);
    // if (con_info == NULL)
    //   return MHD_NO;
    //*con_cls = (void *)con_body; // store pointer for result callback info

    // Get Request Headers
    //RestRequest con_info2(method, url);
    HttpRequest request;
    int noH = MHD_get_connection_values(connection, MHD_HEADER_KIND, request_headers, &request);
    std::cout << "\nREQUEST UPLOAD: head N# " << noH << " size: " << bodySize << " method " << method << " " << url << "\n";
    request.set_body(requestBody);
    // if (con_info2.is_valid_request())
    // {
    //   std::cout << "checked\n";
    // }
    //delete con_info; // request headers read

    try
    {
      // Use the routeHandler to find the static part of url with any vairables.
      // Use the matched static route name to create the HttpRoute object.
      // As that is the callback defined for the matched static url and any varialbe.

      MatchRoute route = rServer->route_handler().match({std::string(method), std::string(url)});

      auto result = route.first->do_route(request);
      std::cout << "CREATED ROUTE RESPONSE\n";
      auto send_result = send_page(connection, result.get());
      return send_result;
    }
    catch (std::exception &e)
    {
      std::unique_ptr<HttpResponse> res2 = std::make_unique<HttpResponse>();

      std::cout << "Unknown url " << url << " " << e.what() << "\n";
      json msg;
      msg["message"] = std::string(e.what()) + " " + std::string(url);
      errs["errors"].push_back(msg);
      res2->set_status(Rest::ResponseIdentity::FileNotFound);
      res2->set_body(errs.dump());
      auto send_result = send_page(connection, res2.get());
      return send_result;
    }

  } // end connection

  // if (0 == strcmp(method, "POST") || 0 == strcmp(method, "PUT"))
  // {
  //   std::cout << "Upload atxxxxx zero and done!\n";

  //   json root;
  //   std::vector<std::string> errList;
  //   bool res = uploadJson(root, requestBody, errList);
  //   if (!res)
  //   {
  //     con_info.setStatus(ResponseIdentity::NotAccepted);
  //     for (auto err : errList)
  //     {
  //       json msg;
  //       msg["message"] = err;
  //       errs["errors"].push_back(msg);
  //     }
  //     con_info.setResult("{\"errors\":\"foo\"}");
  //     std::cout << " " << *upload_data_size << " set err " << errs.dump() << "\n";
  //   }

  //   //  Just sending back what was sent to work
  //   std::string document = root.dump();
  //   std::cout << " " << document << "\n";
  //   con_info.setStatus(ResponseIdentity::OK);
  //   con_info.setResult(document);
  // }
  // else if (0 == strcmp(method, "GET"))
  // {

  //   // ResourceCallReturn result = rServer->doResource(&con_info);
  //   // if (std::get<0>(result))
  //   // {
  //   //   con_info.setStatus(ResponseIdentity::OK);
  //   //   con_info.setResult(std::get<1>(result));
  //   // }
  //   // else
  //   // {
  //   json msg;
  //   msg["message"] = "Unknown url: " + con_info.getUrl();
  //   errs["errors"].push_back(msg);
  //   con_info.setStatus(ResponseIdentity::FileNotFound);
  //   con_info.setResult(errs.dump());
  //   // }
  // }
  // else if (0 == strcmp(method, "DELETE"))
  // {
  //   json msg;
  //   msg["message"] = "Not accepted: " + std::string(method);
  //   errs["errors"].push_back(msg);
  //   con_info.setStatus(ResponseIdentity::NotAccepted);
  //   con_info.setResult(errs.dump());
  // }
  // else
  // { // Other verbs not processed result in NotAccepted status
  //   json msg;
  //   msg["message"] = "Not accepted: " + std::string(method);
  //   errs["errors"].push_back(msg);
  //   con_info.setStatus(ResponseIdentity::NotAccepted);
  //   con_info.setResult(errs.dump());
  // }

  int uploadBody(const char *method,
                 const char *upload_data,
                 size_t *upload_data_size,
                 std::ostringstream *con_body)
  {
    //std::cout << "UPLOAD: " << *upload_data_size << " " << (*upload_data_size == 0) << "\n";
    if (0 == strcmp(method, "PUT") ||
        0 == strcmp(method, "POST"))
    {
      con_body->write(upload_data, *upload_data_size);
      *upload_data_size = 0;
      return MHD_YES;
    }
    else
    {
      return MHD_NO;
    }
  }

  // Send a page
  int send_page(struct MHD_Connection *connection, HttpResponse *con_info)
  {
    int ret = MHD_NO;
    struct MHD_Response *response;
    char *me;
    auto responseID = con_info->status();
    auto pageInfo = con_info->body();

    // Microhttp as a c library need to use malloc/free.
    // The response are made with dynamic content(char).
    // Microhttp uses MHD_RESPMEM_MUST_FREE (IMPORTANT to tell free to cleanup memory.)
    me = (char *)malloc(pageInfo.length() + 1);
    if (me == 0)
      return MHD_NO;
    strncpy(me, pageInfo.c_str(), pageInfo.length() + 1);
    response = MHD_create_response_from_buffer(pageInfo.length(), me, MHD_RESPMEM_MUST_FREE);
    if (!response)
    {
      free(me);
      return MHD_NO;
    }

    // Requirement of microhttpd is to ensure static chars values with MHD_add_response_header()
    for (auto h : con_info->headers())
    {
      ret = MHD_add_response_header(response, h.first.c_str(), h.second.c_str()); // value are strdup
      //ret = MHD_add_response_header(response, h.first.c_str(), h.second.c_str()); // value are strdup

      // char * pName = (char *)malloc(h.first.length() + 1);
      // if (pName == 0)
      //   return MHD_NO;
      // char * pValue = (char *)malloc(h.second.length() + 1);
      // if (pValue == 0)
      //   return MHD_NO;
      // strncpy(pName, h.first.c_str(), h.first.length() + 1);
      // strncpy(pValue, h.second.c_str(), h.second.length() + 1);
      //ret = MHD_add_response_header(response, pName, pValue); // value are strdup
      // free(pName);
      // free(pValue);
      std::cout << "SEND PAGE HEADER: " << h.first << " " << h.second << "\n";
      if (!ret)
      {
        free(me);
        MHD_destroy_response(response);
        return MHD_NO;
      }
    }

    // Use microhttpd defines for response for queue_response function
    switch (responseID)
    {
    case Rest::ResponseIdentity::OK:
      ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
      break;
    case Rest::ResponseIdentity::AuthFail:
      ret = MHD_queue_response(connection, MHD_HTTP_UNAUTHORIZED, response);
      break;
    case Rest::ResponseIdentity::NotAccepted:
      ret = MHD_queue_response(connection, MHD_HTTP_NOT_ACCEPTABLE, response);
      break;
    case Rest::ResponseIdentity::FileNotFound:
      ret = MHD_queue_response(connection, MHD_HTTP_NOT_FOUND, response);
      break;
    case Rest::ResponseIdentity::ServerError:
      ret = MHD_queue_response(connection, MHD_HTTP_INTERNAL_SERVER_ERROR, response);
      break;
    default:
      ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
      break;
    }
    if (!ret)
    {
      free(me);
    }
    MHD_destroy_response(response);
    std::cout << "SEND PAGE QUEUED\n";

    return ret;
  }

  /////////////////////////////////////////
  // File handling in C
  /////////////////////////////////////////
  long get_file_size(const char *filename)
  {
    FILE *fp;

    fp = fopen(filename, "rb");
    if (fp)
    {
      long size;

      if ((0 != fseek(fp, 0, SEEK_END)) || (-1 == (size = ftell(fp))))
        size = 0;

      fclose(fp);

      return size;
    }
    else
      return 0;
  }

  char *load_file(const char *filename)
  {
    FILE *fp;
    char *buffer;
    long size;

    size = get_file_size(filename);
    if (size == 0)
      return NULL;

    fp = fopen(filename, "rb");
    if (!fp)
      return NULL;

    buffer = (char *)malloc(size + 1);
    if (!buffer)
    {
      fclose(fp);
      return NULL;
    }

    if (size != (long)fread(buffer, 1, size, fp))
    {
      free(buffer);
      buffer = NULL;
    }
    buffer[size] = '\0';
    fclose(fp);
    return buffer;
  }

#ifdef __cplusplus
}
#endif

RestServer::RestServer() : m_route_handler{},
                           key_pem(0),
                           cert_pem(0),
                           daemon(0)
{
  std::signal(SIGTERM, signal_handler); // Catch SIGTERM for docker stop to graceful end.
  std::signal(SIGINT, signal_handler);  // Catch Ctrl c to graceful end.
}

RestServer::~RestServer()
{
  std::cout << "Finished server\n";
};

void RestServer::get(const std::string &route, HttpRoute::RouteCall callback)
{
  m_route_handler.declare_route_handler({std::string(Rest::REST_GET), route}, callback);
};

void RestServer::put(const std::string &route, HttpRoute::RouteCall callback)
{
  m_route_handler.declare_route_handler({std::string(Rest::REST_PUT), route}, callback);
};

RestServer::UserKeyCallReturn RestServer::read_user_key(std::string /*username*/)
{
  // This is what is stored in db
  std::string secret{"F72DDA1919D8ECEA6C1684123486CAD94DE783AA43E1C3878C45F38F8E1C1E51"};
  //bool found = true;// false;
  //std::string secret{""};
  bool found = true;
  // Storage::DBStorage db(m_db_desc, m_xml);
  // auto result = db.loginUser("TEST", "pass");
  //   if (true == db.isLoggedUser()) {
  //   	std::vector<User> result = db.doRead<User>({{"UserName", username}}, {});
  //       if (1 == result.size()) {
  // 	   found = true;
  // 	   secret = result.at(0).getSecret();
  //       }
  //   }
  return UserKeyCallReturn(found, secret);
}

int RestServer::run()
{
  auto port = [&]() -> std::uint16_t {
    auto env = std::getenv("PORT");
    if (env == nullptr) return 8888;
    auto value = std::stoi(env);
    if (value < std::numeric_limits<std::uint16_t>::min() ||
        value > std::numeric_limits<std::uint16_t>::max()) {
      std::ostringstream os;
      os << "The PORT environment variable value (" << value
         << ") is out of range.";
      throw std::invalid_argument(std::move(os).str());
    }
    return static_cast<std::uint16_t>(value);
  }();
  // key_pem = load_file("server.key");
  // cert_pem = load_file("server.pem");
  // if ((key_pem == NULL) || (cert_pem == NULL))
  // {
  //   printf("The key/certificate files could not be read.\n");
  //   return 1;
  // }
  // sigset_t base_mask, waiting_mask;
  daemon = MHD_start_daemon(MHD_USE_INTERNAL_POLLING_THREAD,
                            port,
                            NULL, NULL,
                            //                            &RestServer::on_client_connect, NULL,
                            rest_connection, this,
                            //MHD_OPTION_CONNECTION_TIMEOUT
                            MHD_OPTION_END);

  if (NULL == daemon)
  {
    // printf("%s\n", cert_pem);
    // free(key_pem);
    // free(cert_pem);
    std::cout << "Can't start HTTP server (check port " << PORT << " is not in use)\n";
    // }
    return 1;
  }

  std::cout << "Rest Server running " << port << "\n\n";
  while (true)
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    //std::cout << "Wait. ";
    if (gSignalStatus > 0)
    {
      std::cout << "Shutting down on gSignalStatus [" << gSignalStatus << "]\n";
      break;
    }
  }
  std::cout << "Rest Server stopped gracfully\n";

  MHD_stop_daemon(daemon);
  // free(key_pem);
  // free(cert_pem);
  return gSignalStatus;
}

// void RestServer::addResource(std::string path, ResourceCall call)
// {
//   auto pair = std::make_pair(path, call);
//   mapCall.insert(pair);
// }

// bool RestServer::findResourcePath(std::string path)
// {
//   return mapCall.count(path) > 0;
// }

// ResourceCallReturn RestServer::doResource(RestRequest *con_info)
// {
//   json empty;
//   empty = json::array({});
//   std::string url = con_info->getUrl();
//   if (mapCall.count(url) == 0)
//   {
//     return ResourceCallReturn(false, empty.dump());
//   }
//   auto result = mapCall[url](&personalModel, "", "");
//   bool state;
//   std::string item;
//   std::tie(state, item) = result;
//   return result;
// }

// int RestServer::on_client_connect(void * /*cls*/, const struct sockaddr *addr, socklen_t /*addrlen*/)
// {
//   struct sockaddr_in *addr_in = (struct sockaddr_in *)addr;
//   char ipstr[INET6_ADDRSTRLEN];

//   if (inet_ntop(addr_in->sin_family, &addr_in->sin_addr, ipstr, INET6_ADDRSTRLEN) == NULL)
//   {
//     perror("inet_ntop");
//   }
//   else
//   {
//     printf("IP address 1: %s\n", ipstr);
//   }
//   return MHD_YES;
// }
