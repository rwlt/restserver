/*
 * PersonalModel.h
 *
 *  Created on: 20/07/2017
 *      Author: rodney
 */

#ifndef SRC_MODULE_PERSONALMODEL_H_
#define SRC_MODULE_PERSONALMODEL_H_

//#include "Person.h"
//#include "Entity.h"
#include "storage/storage.h"
//#include "ViewIdentity.h"
//#include "validate/ValidationError.h"
#include <string>
#include <vector>
#include <thread>
#include <future>
#include <tuple>

using Storage::DBEntity;
using Storage::DBStorage;
using Storage::Model;
using Storage::Driver::CreateKind;
using Storage::Driver::FieldType;
using PEntity = std::unique_ptr<DBEntity>;

namespace Module
{

  /// PersonalModel
  struct
  {
    struct Table
    {
      std::string People{"People"};
    } T;
    struct Fields
    {
      std::string ID{"P_ID"};
      std::string Name{"Name"};
    } F;
  } PA;

  class PersonalModel
  {
    Schema m_peopleSchema{
        {{PA.F.ID, FieldType::PK_INT},
         {PA.F.Name, FieldType::STRING}}};
    DBStorage m_dbStorage;
    Model m_people;
    std::vector<std::function<void(std::string &, bool)>> m_broadcastPersonChange;

  public:
    PersonalModel(bool create_model = false) : m_dbStorage{{"Sqlite", "localhost", "test.db", "ISO8859_1"}},
                                             m_people{m_dbStorage.model(
                                                 PA.T.People,
                                                 m_peopleSchema,
                                                 CreateKind::NONE)}
    {
      if (create_model)
      {
        m_people = m_dbStorage.model(
            PA.T.People,
            m_peopleSchema,
            CreateKind::DROP_CREATE);
      }
    }
    virtual ~PersonalModel()
    {
    }

    /// Add Broadcast function
    void addBroadcast(std::function<void(std::string &, bool)> func)
    {
      this->m_broadcastPersonChange.push_back(func);
    }
    ///Validate person
    /**
	 * \param person
	 * \return tuple bool and vector ValidationError
	 * Return bool true if valid, empty validation errors.
	 * Return bool false if not valid with person validation errors.
	 */
    //std::vector<Validate::ValidationError> validatePerson(const Module::Person &person);
    /**
	 * \param addPerson string
	 * \return Module::Person
	 * Return when successful the added Person
	 * Throws ModuleException when no logged user or database error in dbStorage object
	 */
    void beginTransaction() { m_dbStorage.beginTransaction(); }
    void endTransaction() { m_dbStorage.commit(); }
    PEntity addNewPerson(int id, const std::string &name);
    /// Change person
    /**
	 * \param origPerson string
	 * \param changePerson string
	 * \return Module::Person
	 * Return when successful the added Person
	 * Throws ModuleException when no logged user or database error in dbStorage object
	 */
    PEntity changePerson(PEntity entity);
    /// Remove person
    //void removePerson(const Storage::Entity &removePerson);

    /// Read the db storage
    /**
	 * Using a template method to retrieve DataType entities in a DBResult
	 * \tparam T entity type mainly
	 * \tparam A entity map
	 * \tparam M entity traits
	 * \return Control::List<T>
	 */
    //template <class T, typename A = Storage::EntityMap<T>, typename M = Storage::EntityTraits<T>>
    std::vector<std::unique_ptr<DBEntity>> doRead()
    {
      //      std::vector<T> dest;

      return m_people.findAll();
      // if (loggedUser())
      // {
      //   Storage::DBRetrieveMap where;
      //   Storage::DBRetrieveMap params;

      //   std::future<std::vector<T>> result(std::async(std::launch::async, &Storage::DBStorage::doRead<T>, &m_dbStorage, where, params));
      //   dest = result.get(); // wait her for async to finish
      // }
      //    return dest;
    }

    // std::vector<Module::Person> doRead()
    // {
    //   std::vector<Module::Person> dest;
    //   //std::this_thread::sleep_for(std::chrono::milliseconds(5000));
    //   if (loggedUser())
    //   {
    //     // Some how need to find when to reset this repo on a new doRead();
    //     if (m_repoPerson.size() > 0)
    //     {
    //       return m_repoPerson.getItems();
    //     }
    //     m_repoPerson.clear();
    //     m_repoPerson.trackStateChanges(false);
    //     Storage::DBRetrieveMap where;
    //     Storage::DBRetrieveMap params;
    //     std::future<std::vector<Module::Person>> result(std::async(std::launch::async, &Storage::DBStorage::doRead<Module::Person>, &m_dbStorage, where, params));
    //     dest = result.get(); // wait her for async to finish
    //     Storage::EntityMap<Module::Person> tent;
    //     int key;
    //     for (Module::Person &t : dest)
    //     {
    //       tent.getField(t, m_repoPerson.getKeyField(), key);
    //       m_repoPerson.addItem(key, t);
    //     }
    //     m_repoPerson.trackStateChanges(true);
    //     return m_repoPerson.getItems();
    //   }
    //   return dest;
    // }
    /// Get Data from a doRead
    //   /**
    //  * \param fraction_done
    //  * \param message
    //  */
    //   void get_data(double *fraction_done, std::string *message) const
    //   {
    //     m_dbStorage.get_data(fraction_done, message);
    //   }
    //   /// Stop Work of a doRead
    //   void stop_work()
    //   {
    //     m_dbStorage.stop_work();
    //   }

  private:
    //	std::string validationMessage(const std::vector<Validate::ValidationError> &errors);
  };

} /* namespace Module */

#endif /* SRC_MODULE_PERSONALMODEL_H_ */
