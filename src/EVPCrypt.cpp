/*
 * EVPCrypt.cpp
 *
 *  Created on: 26/08/2017
 *      Author: rodney
 */

#include "EVPCrypt.h"
#include <openssl/rand.h>
#include <openssl/err.h>


void EVPCrypt::handleErrors(void) {
	ERR_print_errors_fp(stderr);
	abort();
}

void EVPCrypt::gen_params(byte key[KEY_SIZE], byte iv[BLOCK_SIZE]) {
	int rc = RAND_bytes(key, KEY_SIZE);
	if (rc != 1)
		throw std::runtime_error("RAND_bytes key failed");

	rc = RAND_bytes(iv, BLOCK_SIZE);
	if (rc != 1)
		throw std::runtime_error("RAND_bytes for iv failed");
}

void EVPCrypt::aes_encrypt(const byte key[KEY_SIZE], const byte iv[BLOCK_SIZE], const secure_string& ptext,
		secure_string& ctext) {
	EVP_CIPHER_CTX_free_ptr ctx(EVP_CIPHER_CTX_new(), ::EVP_CIPHER_CTX_free);
	int rc = EVP_EncryptInit_ex(ctx.get(), EVP_aes_256_cbc(), NULL, key, iv);
	if (rc != 1)
		throw std::runtime_error("EVP_EncryptInit_ex failed");

	// Recovered text expands upto BLOCK_SIZE
	ctext.resize(ptext.size() + BLOCK_SIZE);
	int out_len1 = (int) ctext.size();

	rc = EVP_EncryptUpdate(ctx.get(), (byte*) &ctext[0], &out_len1, (const byte*) &ptext[0], (int) ptext.size());
	if (rc != 1)
		throw std::runtime_error("EVP_EncryptUpdate failed");

	int out_len2 = (int) ctext.size() - out_len1;
	rc = EVP_EncryptFinal_ex(ctx.get(), (byte*) &ctext[0] + out_len1, &out_len2);
	if (rc != 1)
		throw std::runtime_error("EVP_EncryptFinal_ex failed");

	// Set cipher text size now that we know it
	ctext.resize(out_len1 + out_len2);
}

void EVPCrypt::aes_decrypt(const byte key[KEY_SIZE], const byte iv[BLOCK_SIZE], const secure_string& ctext,
		secure_string& rtext) {
	EVP_CIPHER_CTX_free_ptr ctx(EVP_CIPHER_CTX_new(), ::EVP_CIPHER_CTX_free);
	int rc = EVP_DecryptInit_ex(ctx.get(), EVP_aes_256_cbc(), NULL, key, iv);
	if (rc != 1)
		throw std::runtime_error("EVP_DecryptInit_ex failed");

	// Recovered text contracts upto BLOCK_SIZE
	rtext.resize(ctext.size());
	int out_len1 = (int) rtext.size();

	rc = EVP_DecryptUpdate(ctx.get(), (byte*) &rtext[0], &out_len1, (const byte*) &ctext[0], (int) ctext.size());
	if (rc != 1)
		throw std::runtime_error("EVP_DecryptUpdate failed");

	int out_len2 = (int) rtext.size() - out_len1;
	rc = EVP_DecryptFinal_ex(ctx.get(), (byte*) &rtext[0] + out_len1, &out_len2);
	if (rc != 1)
		throw std::runtime_error("EVP_DecryptFinal_ex failed");

	// Set recovered text size now that we know it
	rtext.resize(out_len1 + out_len2);
}

int EVPCrypt::encryptccm(unsigned char *plaintext, int plaintext_len, unsigned char *aad, int aad_len, unsigned char *key,
		unsigned char *iv, unsigned char *ciphertext, unsigned char *tag) {
	EVP_CIPHER_CTX *ctx;

	int len;
	//EVP_aes_256_ccm
	int ciphertext_len;

	/* Create and initialise the context */
	if (!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	/* Initialise the encryption operation. */
	if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ccm(), NULL, NULL, NULL))
		handleErrors();

	/* Setting IV len to 7. Not strictly necessary as this is the default
	 * but shown here for the purposes of this example */
	if (1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_IVLEN, 7, NULL))
		handleErrors();

	/* Set tag length */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_TAG, 14, NULL);

	/* Initialise key and IV */
	if (1 != EVP_EncryptInit_ex(ctx, NULL, NULL, key, iv))
		handleErrors();
	//	OPENSSL_assert(EVP_CIPHER_CTX_key_length(ctx) == 16);
	//	OPENSSL_assert(EVP_CIPHER_CTX_iv_length(ctx) == 16);

	/* Provide the total plaintext length
	 */
	if (1 != EVP_EncryptUpdate(ctx, NULL, &len, NULL, plaintext_len))
		handleErrors();

	/* Provide any AAD data. This can be called zero or one times as
	 * required
	 */
	if (1 != EVP_EncryptUpdate(ctx, NULL, &len, aad, aad_len))
		handleErrors();

	/* Provide the message to be encrypted, and obtain the encrypted output.
	 * EVP_EncryptUpdate can only be called once for this
	 */
	if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
		handleErrors();
	ciphertext_len = len;

	/* Finalise the encryption. Normally ciphertext bytes may be written at
	 * this stage, but this does not occur in CCM mode
	 */
	if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
		handleErrors();
	ciphertext_len += len;

	/* Get the tag */
	if (1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_GET_TAG, 14, tag))
		handleErrors();

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);

	return ciphertext_len;
}

int EVPCrypt::decryptccm(unsigned char *ciphertext, int ciphertext_len, unsigned char *aad, int aad_len, unsigned char *tag,
		unsigned char *key, unsigned char *iv, unsigned char *plaintext) {
	EVP_CIPHER_CTX *ctx;
	int len;
	int plaintext_len;
	int ret;

	/* Create and initialise the context */
	if (!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	/* Initialise the decryption operation. */
	if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_ccm(), NULL, NULL, NULL))
		handleErrors();

	/* Setting IV len to 7. Not strictly necessary as this is the default
	 * but shown here for the purposes of this example */
	if (1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_IVLEN, 7, NULL))
		handleErrors();

	/* Set expected tag value. */
	if (1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_TAG, 14, tag))
		handleErrors();

	/* Initialise key and IV */
	if (1 != EVP_DecryptInit_ex(ctx, NULL, NULL, key, iv))
		handleErrors();

	/* Provide the total ciphertext length
	 */
	if (1 != EVP_DecryptUpdate(ctx, NULL, &len, NULL, ciphertext_len))
		handleErrors();

	/* Provide any AAD data. This can be called zero or more times as
	 * required
	 */
	if (1 != EVP_DecryptUpdate(ctx, NULL, &len, aad, aad_len))
		handleErrors();

	/* Provide the message to be decrypted, and obtain the plaintext output.
	 * EVP_DecryptUpdate can be called multiple times if necessary
	 */
	ret = EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len);

	plaintext_len = len;

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);

	if (ret > 0) {
		/* Success */
		return plaintext_len;
	} else {
		/* Verify failed */
		return -1;
	}
}
