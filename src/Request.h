#ifndef SRC_RESTREQUEST_H
#define SRC_RESTREQUEST_H

#include "Content.h"
#include "Parameters.h"
#include "Headers.h"
#include <string>
#include <iostream>
namespace Rest
{
    
  /// Identity of response to return
  enum RestVerb
  {
    Get = 0,  ///< OK response
    Post = 1, ///< Authorization wrong
    Put = 2,  ///< Content format in error
    Delete = 3,
    Other = 4
  };

  class Request: public Content, public Headers
  {
   
  public:
    virtual ~Request() {
    };

    virtual std::string response_id() = 0;
    virtual const Parameters& params() const = 0; 
  };

}

#endif /* SRC_RESTREQUEST_H */