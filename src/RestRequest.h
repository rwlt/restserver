/*
 * RestRequest.h
 *
 *  Created on: 26/08/2017
 *      Author: rodney
 */

#ifndef SRC_APP_RESTREQUEST_H_
#define SRC_APP_RESTREQUEST_H_

#include <microhttpd.h>
#include <string>
#include <vector>
#include <tuple>
#include "hashalgs/DateTimeStamp.h"
#include "nholmann/json.hpp"
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <cstring>

using json = nlohmann::json;

/// Identity of response to return
enum ResponseIdentity
{
  OK = 0,          ///< OK response
  AuthFail = 1,    ///< Authorization wrong
  NotAccepted = 2, ///< Content format in error
  FileNotFound = 3,
  ServerError = 4
};

/// Identity of response to return
enum RestVerb
{
  Get = 0,  ///< OK response
  Post = 1, ///< Authorization wrong
  Put = 2,  ///< Content format in error
  Delete = 3,
  Other = 4
};

constexpr const char *HMAC_TOKEN = "hmac ";

/// Class ResetRequest
class RestRequest
{
  RestVerb m_restverb{RestVerb::Get};
  std::string m_url{""};
  char *m_pPage{nullptr};
  int m_pageSize{0};
  std::stringstream m_upload{};
  bool m_uploaded{false}; // to know if empty body after recv PUT or POST
  std::string m_date{""};
  std::string m_user{""};
  std::string m_nonce{""};
  std::string m_hashRecB64{""};
  MHD_PostProcessor *m_postprocessor{0};
  std::string m_answerstring{"[]"};

  // bool m_failed{false};
  ResponseIdentity m_responseID{ResponseIdentity::OK};
  std::unordered_map<std::string, std::string> m_headers{{"Content-Type", "application/json"}};

public:
  typedef std::tuple<DateTimeStamp, std::string> RequestTimeStamp;
  const char *askpage =
      "<html><body>\
	                       What's your name, Sir?<br>\
	                       <form action=\"/namepost\" method=\"post\">\
	                       <input name=\"name\" type=\"text\"\
	                       <input name=\"resturl\" type=\"text\"\
	                       <input type=\"submit\" value=\" Send \"></form>\
	                       </body></html>";
  const char *greatingpage = "<html><body><h1>Welcome, %s!</center></h1></body></html>";
  static const char *errorpageExpired;
  static const char *errorpageRepeated;
  static const char *errorpageUnknownUser;
  static const char *errorpageRequestError;
  static const char *errorpage;

public:
  RestRequest(std::string method,
              std::string url) : m_url(url),
                                 m_pPage{nullptr},
                                 m_pageSize{0},
                                 m_upload{},
                                 m_uploaded{false}
  {

    //		std::stringstream test(url);
    //		std::string segment;
    //		std::vector<std::string> seglist;
    //		while (std::getline(test, segment, '/')) {
    //			if (segment.compare("") != 0)
    //				seglist.push_back(segment);
    //		}
    //		if (seglist.size() > 0) {
    //			// Can we create the rest request from segment 1
    //
    //		}

    if (method.compare("POST") == 0)
    {
      //			MHD_PostProcessor *postprocessor = MHD_create_post_processor(connection, POSTBUFFERSIZE, iterate_post, (void*) con_info);
      //			if (NULL == postprocessor) {
      //				delete con_info;
      //				return MHD_NO;
      //			}
      //			con_info->setMHDPostProcessor(postprocessor);
      m_restverb = RestVerb::Post;
    }
    else if (method.compare("GET") == 0 ||
             method.compare("PUT") == 0 ||
             method.compare("DELETE") == 0)
    {
      if (method.compare("GET") == 0)
        m_restverb = RestVerb::Get;
      if (method.compare("PUT") == 0)
        m_restverb = RestVerb::Put;
      if (method.compare("DELETE") == 0)
        m_restverb = RestVerb::Delete;
    }
    else
    {
      m_restverb = RestVerb::Other;
    }
  }

  ~RestRequest()
  {
    // if (m_pPage)
    // {
    //   delete[] m_pPage;
    //   std::cerr << "RestRequest delete m_pPage\n";
    //   m_pPage = nullptr;
    // }
    if (m_postprocessor != 0)
    {
      std::cerr << "~RestRequest processor\n";
      MHD_destroy_post_processor(m_postprocessor);
      m_postprocessor = 0;
    }
  }

  void setMHDPostProcessor(MHD_PostProcessor *postprocessor)
  {
    m_postprocessor = postprocessor;
  }

  MHD_PostProcessor *getMHDPostProcessor()
  {
    return m_postprocessor;
  }

  void setAuthorization(const std::string &hauth)
  {
    std::vector<std::string> parts = split(hauth, ':');
    if (parts.size() == 3)
    {
      m_user = parts.at(0);
      m_nonce = parts.at(1); // We only accept the nonce once for the request time
      m_hashRecB64 = parts.at(2);
    }
  }

  void setDate(const char *date)
  {
    m_date = std::string(date);
  }

  void setStatus(ResponseIdentity responseID)
  {
    m_responseID = responseID;
    //m_failed = true;
  }

  bool isPost()
  {
    return (m_restverb == RestVerb::Post);
  }

  RestVerb getRestVerb()
  {
    return (m_restverb);
  }

  std::pair<int, const char *> getResult()
  {
    //return {m_pageSize, m_pPage};
    return {m_answerstring.length(), m_answerstring.c_str()};
  }
  void setResult(std::string result)
  {
    //std::cerr << "RestRequest setResult m_pPage " << result << "\n";

    // if (m_pPage)
    // {
    //   std::cerr << "RestRequest setResult delete an m_pPage \n";
    //   delete[] m_pPage;
    //   m_pPage = nullptr;
    // }
    // m_pPage = new char[result.length() + 1];
    // strcpy(m_pPage, result.c_str());
    // m_pPage[result.length()] = '\0';
    // m_pageSize = result.length();

    m_answerstring = result;
  }

  void upload(const char *upload_data,
              size_t upload_data_size)
  {
    m_upload.write(upload_data, upload_data_size);
    //    if (!m_upload.good())
  }

  void setUploaded(bool value)
  {
    m_uploaded = value;
    m_upload.put('\0'); // end of upload data
  }

  bool isUpLoaded() { return m_uploaded; }

  bool uploadJson(json &root, 
                  std::vector<std::string> &errs)
  {
    if (!m_upload.good())
    {
      errs.push_back("error in upload to stream");
      return false;
    }
    try
    {
      root = json::parse(m_upload.str());
    }
    catch (json::exception &e)
    {
      errs.push_back(std::to_string(e.id) + " " + e.what());
      return false;
    }
    return true;
  }

  std::string getUserName()
  {
    return m_user;
  }

  std::string getUserNonce()
  {
    return m_nonce;
  }

  ResponseIdentity getResponseIdentity()
  {
    return m_responseID;
  }

  std::unordered_map<std::string, std::string> &getHeaders()
  {
    return m_headers;
  }

  std::string getUrl()
  {
    return m_url;
  }

  bool is_valid_request();
  bool is_hmac_authorized(std::string secret);

  RequestTimeStamp getDateTimeStamp()
  {
    DateTimeStamp dts(m_date);
    RequestTimeStamp ret(dts, getUserNonce());
    return ret;
  }

private:
  std::vector<std::string> split(const std::string &s, char delim);
  std::string connectiontype();
};

#endif /* SRC_APP_RESTREQUEST_H_ */
